<?php
// Ici un tableau des produits disponible à la vente chez KFC
$products = [
    [
        "id" => 1,
        "nom" => "Tower bacon",
        "prix" => 12.2,
        "photo" => "https://i.ytimg.com/vi/QuDvbVz80BI/sddefault.jpg"
    ],
    [
        "id" => 2,
        "nom" => "Bucket pour 2",
        "prix" => 9.99,
        "photo" => "https://static.kfc.fr/images/items/lg/Bucket-16HW.jpg?v=Lnokl4"
    ],
    [
        "id" => 3,
        "nom" => "Burger Veggie",
        "prix" => 9.90,
        "photo" => "https://jai-un-pote-dans-la.com/wp-content/uploads/2022/08/havasparis-kfc-premierburgerveggie.jpg"
    ]
];

// Si j'ai cliqué sur le lien ajouter au panier, je dois passer dans cette condition
if (array_key_exists("action", $_GET) && $_GET["action"] == 'panier') {
    $panier = [];
    // Je vérifie si j'ai déjà un panier stocké dans mes cookies
    if (array_key_exists("panier", $_COOKIE)) {
        // Comme un cookie ne peut contenir que du texte, je transforme
        // le texte contenu dans mes cookies en tableau associatif
        $panier = json_decode($_COOKIE["panier"], true);
    }

    // Je réccupére l'ID du produit que je souhaite ajouter dans mon panier
    $id = $_GET["id"];

    // Je parcours tous mes produits à la recherche du produit à ajouter au panier
    foreach ($products as $product) {
        // Si le produit correspond à celui que je veux ajouter :
        if ($id == $product["id"]) {
            $cle = null;
            // Je regarde si il est pas présent dans mon panier
            foreach ($panier as $key => $val) {
                // Si il est présent, je passe dans cette condition
                // afin de retrouver sa clé (position dans le tableau du panier)
                if ($val["id"] == $product["id"]) {
                    // Je récupère sa clé
                    $cle = $key;
                }
            }

            // Si ma clé n'est pas nulle, ça veut dire qu'il est déjà dans le
            // panier ... Donc j'augmante simplement la quantité
            if (!is_null($cle)) {
                $panier[$cle]["quantite"] = $panier[$cle]["quantite"] + 1;
            } else {
                // Sinon j'ajoute le produit qui aura une quantité à 1
                $product["quantite"] = 1;
                $panier[] = $product;
            }
        }
    }

    // Pour finir, j'ajoute dans mes cookies le panier.
    // Les cookie ne pouvant stocker que des chaines de caractères,
    // je transforme mon panier en chaine de caractères.
    setcookie("panier", json_encode($panier));
}
?>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <?php
    include "nav.php"
    ?>

    <h1>Borne de commande !</h1>
    <div class="row">
        <?php
        // Je parcours tous les produits en vente pour les afficher
        foreach ($products as $product) {
            echo('        <div class="card" style="width: 18rem;">
            <img src="' . $product["photo"] . '" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">' . $product["nom"] . '</h5>
                <a href="admin-index.php?id=' . $product["id"] . '&action=panier" class="btn btn-primary">Ajouter au panier</a>
            </div>
        </div>');
        }
        ?>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
</body>
</html>