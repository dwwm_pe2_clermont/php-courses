<?php
    // session_start();
?>
<html>
<head>
    <?php
        include 'parts/stylesheets.php';
    ?>
</head>
<body>
<div class="container">
<?php
    include "parts/nav.php";
?>
<h1>Mes photos</h1>
    <div class="row">
        <?php
        $images = scandir("uploads");

        foreach ($images as $image){
            if($image != '.' && $image != '..'){
                echo('<div class="card" style="width: 18rem;">
  <img src="uploads/'.$image.'" class="card-img-top" alt="...">
    </div>');
            }
        }

        ?>
    </div>
    <?php
        include "parts/cookie-content.php";
    ?>
</div>


<?php
    include 'parts/scripts.php'
?>
</body>
</html>