<?php
    // session_start

    $errors = [];
    // Je vérifie si le formulaire de connexion à été soumis
    if($_SERVER["REQUEST_METHOD"] == 'POST'){
        // Je réccupère la valeur saisie dans username
        $username = $_POST["username"];
        // Je réccupère la valeur saisie dans le mot de passe
        $password = $_POST["password"];

        // J'essaie de me connecter à ma base de données (pensez à adapter à votre source de données).
        try {
            $host = 'database';
            $dbName = 'authent';
            $user = 'root';
            $passwordDb = 'tiger';

            $pdo = new PDO(
                'mysql:host='.$host.';dbname='.$dbName.';charset=utf8',
                $user,
                $passwordDb);
            // Cette ligne demandera à pdo de renvoyer les erreurs SQL si il y en a
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e) {
            throw new InvalidArgumentException('Erreur connexion à la base de données : '.$e->getMessage());
            exit;
        }

        // Je selectionne l'utilisateur qui correspond à cet username.
        $request =
            $pdo->prepare("SELECT * FROM users WHERE username = :username");
        $resultats = $request->execute(["username"=> $username]);

        // Je réccupére l'utilisateur dans un tableau clé => valeur
        $resultats = $request->fetch();

        // Si je n'ai pas de resultat, j'ajoute un message d'erreur.
        // Je ne suis pas très précis dans la cause pour éviter le bruteforce
        if(!$resultats){
            $errors[] = "Identifiant ou mot de passe incorrecte";
        } else {
            // Si l'utilisateur existe, je vérifie le mot de passe
            if(password_verify($password, $resultats["password"])){
                // Si c'est OK, j'ajoute mon utilisateur en session
                $_SESSION["username"] = $resultats["username"];
                // Je le redirige vers la page d'acceuil
                header("Location: admin-index.php");
                exit();
            } else {
                // Sinon le mot de passe est pas bon
                // mais je donne un message d'erreur vague pour éviter le
                // bruteforce
                $errors[] = "Identifiant ou mot de passe incorrecte";
            }
        }

    }
?>
<html>
<head>

</head>
<body>
<h1>Me connecter !</h1>

<form method="post">
    <div>
        <label>Username</label>
        <input type="text" placeholder="Username" name="username">
    </div>

    <div>
        <label>Password</label>
        <input type="password" name="password" placeholder="password">
    </div>

    <?php
        foreach ($errors as $error){
            echo('<div style="color: red">'.$error.'</div>');
        }
    ?>
    <input type="submit">
</form>
</body>
</html>