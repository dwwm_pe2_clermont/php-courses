<?php
class CarController{

    private $carManager;

    public function __construct(){
        $this->carManager = new CarManager();
    }
    public function list(){
        $cars =$this->carManager->getAll();

        require "View/cars/list.php";
    }

    public function add(){
        $errors = [];

        if($_SERVER["REQUEST_METHOD"] == "POST"){
            $errors = $this->isValid();

            if(count($errors) == 0){
                $car = new Car(null, $_POST["marque"],
                    $_POST["model"], $_POST["energy"],
                    ($_POST["automatic"]=='on')?1:0, null);

                if($_FILES["picture"]["size"] != 0){
                   $filename = uniqid().$_FILES["picture"]['name'];
                   $car->setPicture($filename);
                   move_uploaded_file($_FILES["picture"]["tmp_name"], "Public/uploads/".$filename);
                }

                $this->carManager->insert($car);
                header("Location: index.php?controller=car&action=list");
            }
        }

        require "View/cars/form.php";
    }

    private function isValid(){
        $errors = [];

        if(empty($_POST["marque"])){
            $errors["marque"] = "Veuillez saisir une marque";
        } else if(strlen($_POST["marque"])>250){
            $errors["marque"] = "La marque est trop longue (250 caractères)";
        }

        if(empty($_POST["model"])){
            $errors["model"] = "Veuillez saisir un modèle";
        } else if(strlen($_POST["model"])>250){
            $errors["model"] = "Le modèle est trop long (250 caractères)";
        }

        if(empty($_POST["energy"])){
            $errors["energy"] = "Veuillez selectionner l'energie";
        } else if(!in_array($_POST["energy"], Car::$allowedEnergy)){
            $errors["energy"] = "Cette energie n'existe pas";
        }

        if(array_key_exists("picture", $_FILES)){
            $image = $_FILES["picture"];
            $allowedFormat = ["image/jpeg", "image/png"];

            if($image["size"]>0){
                if(!in_array($image["type"], $allowedFormat)){
                    $errors["picture"] = "L'image n'est pas au bon format";
                }

                if($image["size"]> 250000000){
                    $errors["picture"] = "Le fichier est trop gros";
                }

                if($image["error"] != 0 && $image["error"] != 4){
                    $errors["picture"] = "Une erreur inconnue s'est produite";
                }
            }

        }

        return $errors;
    }

    public function detail($id)
    {
        $car = $this->carManager->getOne($id);

        require 'View/cars/detail.php';
    }

    public function delete($id)
    {
        $this->carManager->delete($id);
        header("Location: index.php?controller=car&action=list");
    }
}