<?php
class PokemonManager extends DbManager implements CrudInterface {

    public function findAll(){
        $query = $this->bdd->query("SELECT * FROM Pokemon");
        $resultats = $query->fetchAll();

        $arrayObject = [];

        foreach ($resultats as $resultat){
            $arrayObject[] = new Pokemon(
                $resultat["id"],
                $resultat["image"],
                $resultat["nom"],
                $resultat["pv"],
                $resultat["attaque"],
                $resultat["defense"],
                $resultat["vitesse"],
                $resultat["special"],
                $resultat["type_id"]);
        }

        return $arrayObject;

    }
    public function find($id, $deep=false){

        if(!$deep) {
            $query = $this->bdd->prepare("SELECT * FROM Pokemon WHERE id = :id");
            $query->execute(["id" => $id]);
            $resultatTableau = $query->fetch();

            $pokemon = null;

            if ($resultatTableau) {
                $pokemon = new Pokemon(
                    $resultatTableau["id"],
                    $resultatTableau["image"],
                    $resultatTableau["nom"],
                    $resultatTableau["pv"],
                    $resultatTableau["attaque"],
                    $resultatTableau["defense"],
                    $resultatTableau["vitesse"],
                    $resultatTableau["special"],
                    $resultatTableau["type_id"]);
            }
        } else {
            $query = $this->bdd->prepare(
                "SELECT * FROM Pokemon p  JOIN type t  ON p.type_id = t.id WHERE p.id = :id
                   "
            );

            $query->execute(["id" => $id]);
            $resultatTableau = $query->fetch();

            if($resultatTableau){

                $type = new Type($resultatTableau["type_id"], $resultatTableau['libelle'], $resultatTableau["color"]);

                $pokemon = new Pokemon(
                    $resultatTableau["id"],
                    $resultatTableau["image"],
                    $resultatTableau["nom"],
                    $resultatTableau["pv"],
                    $resultatTableau["attaque"],
                    $resultatTableau["defense"],
                    $resultatTableau["vitesse"],
                    $resultatTableau["special"],
                    $type);
            }
        }
        return $pokemon;
    }

    public function save($pokemon){
        $query = $this->bdd->prepare(
            "INSERT INTO pokemon 
        (image, nom, pv, attaque, defense, vitesse, special, type_id)
        VALUES (:image, :nom, :pv, :attaque, :defense, :vitesse, :special, :type);"
        );

        $query->execute([
            "image"=> $pokemon->getImage(),
            "nom"=> $pokemon->getNom(),
            "pv"=> $pokemon->getPv(),
            "attaque"=> $pokemon->getAttaque(),
            "defense"=> $pokemon->getDefense(),
            "vitesse"=> $pokemon->getVitesse(),
            "special"=> $pokemon->getSpecial(),
            "type"=> $pokemon->getType()
        ]);
    }

    public function remove($pokemon){
        $query = $this->bdd->prepare("DELETE FROM pokemon WHERE id = :id");
        $query->execute([
            "id"=> $pokemon->getId()
        ]);
    }

    public function edit($pokemon){
        $request = $this->bdd->prepare("UPDATE pokemon SET
        image = :image, 
        nom = :nom, 
        pv = :pv, 
        attaque = :attaque, 
        defense = :defense, 
        vitesse = :vitesse,
        special = :special, 
        type_id = :type
        WHERE id = :id");

        $request->execute([
            "image"=> $pokemon->getImage(),
            "nom" => $pokemon->getNom(),
            "pv"=> $pokemon->getPv(),
            "attaque"=> $pokemon->getAttaque(),
            "defense"=> $pokemon->getDefense(),
            "vitesse"=> $pokemon->getVitesse(),
            "special"=> $pokemon->getSpecial(),
            "type"=> $pokemon->getType(),
            "id"=> $pokemon->getId()
        ]);


    }

    public function sayHello()
    {
        // TODO: Implement sayHello() method.
    }
}