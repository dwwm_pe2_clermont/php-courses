<?php
    class TechnosController extends AdminController {
        private $technoManager;

        public function __construct(){
            parent::__construct();
            $this->technoManager = new TechnoManager();
        }

        // Affiche toutes les technos
        public function getAllTechnos(){
            // Appeler model pour réccupérer toutes les technos
            $allTechnologies = $this->technoManager->getAll();
            // Afficher la vue qui liste les technos
            require "View/technos/list.php";
        }

        public function detail($id){
            $techno = $this->technoManager->getOne($id);

            require "View/technos/detail.php";
        }

        public function remove($id){
            // Appeler mon manager pour supprimer la données
            $this->technoManager->remove($id);
            // Rediriger vers la homepage
            header("Location: index.php?controller=techno&action=list");
        }

        public function add(){
            $errors = [];

            if($_SERVER["REQUEST_METHOD"] == "POST"){
                // Vérifier les données du formulaire
                $errors = $this->isValid();
                // Enregistrer les données via mon manager si les données sont valides
                if(count($errors) == 0){
                    $techno = new Techno(null, $_POST["nom"],
                        $_POST["image"], $_POST["categorie"]);
                    $this->technoManager->insert($techno);
                    header("Location: index.php?controller=techno&action=list");
                }
            }

            require 'View/technos/form.php';
        }

        private function isValid(){
            $errors = [];
            if(empty($_POST["nom"])){
                $errors["nom"] = "Veuillez saisir un nom";
            }

            if(empty($_POST["image"])){
                $errors["image"] = "Veuillez saisir une image";
            }

            if(!in_array($_POST["categorie"], Techno::$allowedTechno)){
                $errors["categorie"] = "Cette catégorie n'existe pas";
            }

            if(strlen($_POST["image"])>250){
                $errors["image"] = "Ce lien est trop long !";
            }

            return $errors;
        }

        public function edit($id)
        {
            $errors = [];
            $technoEdit = $this->technoManager->getOne($id);

            if($_SERVER["REQUEST_METHOD"] == "POST"){
                $technoEdit->setNom($_POST["nom"]);
                $technoEdit->setImage($_POST["image"]);
                $technoEdit->setCategory($_POST["categorie"]);

                $errors = $this->isValid();
                if(count($errors) == 0){
                    $this->technoManager->update($technoEdit);
                    header("Location: index.php?controller=techno&action=list");
                }
            }

            require 'View/technos/edit.php';
        }
    }