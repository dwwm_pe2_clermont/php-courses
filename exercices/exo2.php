<?php
    // Je cré un tableau vide
    $nombres = [];

    // J'ajoute dans ce tableau 5 nombres aléatoire
    for ($i=0; $i<5; $i++){
        $nombres[] = rand(0,100);
    }

    // La fonction min retourne le plus petit nombre d'un tableau
    echo('La plus petite valeur est : ' . min($nombres).'</br>');
    // La fonction max retourne la plus grande valeur d'un tableau
    echo('La plus petite valeur est : ' . max($nombres).'</br>');

    // Pour calculer la moyenne, je réccupére la somme de tous mes
    // nombres avec la fonctions array_sum
    // Je divise par le nombre d'éléments
    $moyenne = array_sum($nombres) / count($nombres);
    echo('La moyenne des nombres du tableau est : ' . $moyenne.'</br>');

    // Pour trier des nombres de manière croissante dans un tableau
    // J'utilise la fonction php asort. Elle en paramètre le tableau à trier
    asort($nombres);
    var_dump($nombres);