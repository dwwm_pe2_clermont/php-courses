<?php
    class Guerrier extends Personnage{
        public function attaquer(){
            echo('Je suis un guerrier ');
            parent::attaquer();
            echo(' avec une épée');
        }

        public function deplacer()
        {
            echo('Je suis un guerrier ');
            parent::deplacer();
            echo(' a pied');
        }
    }