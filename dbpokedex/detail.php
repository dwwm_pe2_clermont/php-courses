<?php
    // J'ai créé un fichier de fonctions
    include 'functions.php';

    checkConnection();

    // J'appel la méthode db connect qui est dans ce fichier de fonction

    // Elle retournera la connexion à ma base de données
    $pdo = dbConnect();

    // Je réccupére l'id dans mon URL
    $id = $_GET["id"];

    // Je selectionne avec une requête préparé le pokemon qui a l'identifiant
    // compris dans l'URL
    $query = $pdo->prepare("SELECT * FROM pokemon WHERE id = :id");
    $query->execute(["id"=> $id]);
    // Je réccupére mon resultat dans une variable
    $resultat = $query->fetch();

?>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous"/>
</head>
<body>

<div class="container">
    <?php
        include 'parts/navbar.php'
    ?>
    <a href="index.php">Retour</a>

    <?php
    // Si il n'y a pas de résultat, j'affiche un message qui indique que le pokemon n'existe pas
     if(!$resultat){
         echo('<div class="text-danger text-center">Ce pokémon n\'existe pas il a peut être été supprimé </div>');
     }
     // Sinon, j'affiche la page du pokemon
     else {
    ?>
    <h1>Détail du pokemon <?php echo($resultat["nom"]); ?> !</h1>
    <div class="row">

        <img class="m-auto" style="max-width: 300px" src="uploads/images_pokemon/<?php echo($resultat["image"]);?>" alt="Photo de <?php echo($resultat["nom"]); ?>">

    </div>

    <div class="row">
        <div class="col-md-12">
            <h2>Points de vie : <?php echo($resultat["pv"]);?></h2>
        </div>
        <div class="col-md-12">
            <div class="progress">
                <div class="progress-bar bg-success" role="progressbar"
                     style="width: <?php echo($resultat["pv"]/200*100);?>%"
                     aria-valuenow="<?php echo($resultat["pv"]);?>"
                     aria-valuemin="0" aria-valuemax="200"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2>Points d'attaque : <?php echo($resultat["attaque"]);?></h2>
        </div>
        <div class="col-md-12">
            <div class="progress">
                <div class="progress-bar bg-info" role="progressbar" style="width: <?php echo($resultat["attaque"]/200*100);?>%" aria-valuenow="<?php echo($resultat["attaque"]);?>" aria-valuemin="0" aria-valuemax="200"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2>Points de défense : <?php echo($resultat["defense"]);?></h2>
        </div>
        <div class="col-md-12">
            <div class="progress">
                <div class="progress-bar bg-danger" role="progressbar" style="width: <?php echo($resultat["defense"]/200*100);?>%" aria-valuenow="<?php echo($resultat["defense"]);?>" aria-valuemin="0" aria-valuemax="200"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2>Points vitesse : <?php echo($resultat["vitesse"]);?></h2>
        </div>
        <div class="col-md-12">
            <div class="progress">
                <div class="progress-bar bg-info" role="progressbar" style="width: <?php echo($resultat["vitesse"]/200*100);?>%" aria-valuenow="<?php echo($resultat["vitesse"]);?>" aria-valuemin="0" aria-valuemax="200"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2>Points vitesse : <?php echo($resultat["special"]);?></h2>
        </div>
        <div class="col-md-12">
            <div class="progress">
                <div class="progress-bar bg-warning" role="progressbar" style="width: <?php echo($resultat["special"]/200*100);?>%" aria-valuenow="<?php echo($resultat["special"]);?>" aria-valuemin="0" aria-valuemax="200"></div>
            </div>
        </div>
    </div>
    <?php
     }
    ?>

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>

