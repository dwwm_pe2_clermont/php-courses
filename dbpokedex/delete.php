<?php
    // On inclut le fichier de fonction
    require 'functions.php';

    checkConnection();

    // Ceci nous permet d'appeler la fonction dbConnect qui
    // nous permettra de se connecter à la base de données
    $pdo = dbConnect();

    // Je fais une requête préparée pour éviter les injections SQL
    $query = $pdo->prepare("DELETE FROM pokemon WHERE id = :id");
    // J'execute ma requête
    $query->execute(["id"=> $_GET["id"]]);

    // Mon pokemon est supprimé, je peux rediriger vers la page d'index
    header("Location: admin-index.php");
?>