<?php
    $errors = [];

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        // Vérifier que le username est bien saisie
        if(empty($_POST["username"])){
            $errors["username"] = "Veuillez saisir un nom d'utilisateur";
        }

        // Vérifier que l'email est bien saisie
        if(empty($_POST["email"])){
            $errors["email"] = "Veuillez saisir un email";
            // Vérifier que l'email est valide
        } elseif (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
            $errors["email"] = "L'email n'est pas valide !";
        }

        // Vérifier la complexité du mot de passe
        if(empty($_POST["password"])){
            $errors["password"] = "Veuillez saisir un mot de passe";
        } elseif (!preg_match('/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/',
            $_POST["password"])){
            $errors["password"] = "Veuillez saisir au moins 8 caractères, un chiffre et une lettre";
        }
    // Vérifier que les 2 mots de passe sont indentique !

        if($_POST["password"] != $_POST["confirm_password"]){
            $errors["confirm_password"] = "Les mots de passes sont différents";
        }

        if(count($errors) == 0){
            header('Location: ok.php');
            exit();
        }
    }


?>
<html>
<head>
    <?php
    include 'parts/stylesheets.php';
    ?>
</head>
<body>
<div class="container">
    <?php
    include 'parts/nav.php'
    ?>
    <h1>Créer mon compte</h1>
    <form method="post">
        <div class="form-group">
            <label>Username</label>
            <input class="form-control <?php
                if(array_key_exists("username", $errors)){
                    echo('is-invalid');
                } elseif (!array_key_exists("username", $errors)
                    && $_SERVER["REQUEST_METHOD"] == 'POST'){
                    echo('is-valid');
                }
            ?>" value="<?php if(array_key_exists("username", $_POST)){
                echo($_POST["username"]);
            }?>" type="text" name="username">

            <?php
            if(array_key_exists("username", $errors)){
                echo(' <div class="invalid-feedback">
                '.$errors["username"].'
            </div>');
            }
            ?>

        </div>

        <div class="form-group">
            <label>Email</label>
            <input class="form-control <?php
            if(array_key_exists("email", $errors)){
                echo('is-invalid');
            } elseif (!array_key_exists("email", $errors)
                && $_SERVER["REQUEST_METHOD"] == 'POST'){
                echo('is-valid');
            }
            ?>" value="<?php if(array_key_exists("email", $_POST)){
                echo($_POST["email"]);
            }?>" type="text" name="email">

            <?php
            if(array_key_exists("email", $errors)){
                echo(' <div class="invalid-feedback">
                '.$errors["email"].'
            </div>');
            }
            ?>
        </div>

        <div class="form-group">
            <label>Mot de passe</label>
            <input class="form-control <?php
            if(array_key_exists("password", $errors)){
                echo('is-invalid');
            } elseif (!array_key_exists("password", $errors)
                && $_SERVER["REQUEST_METHOD"] == 'POST'){
                echo('is-valid');
            }
            ?>" type="password" name="password">

            <?php
            if(array_key_exists("password", $errors)){
                echo(' <div class="invalid-feedback">
                '.$errors["password"].'
            </div>');
            }
            ?>
        </div>

        <div class="form-group">
            <label>Confirmation du mot de passe</label>
            <input class="form-control <?php
            if(array_key_exists("confirm_password", $errors)){
                echo('is-invalid');
            } elseif (!array_key_exists("confirm_password", $errors)
                && $_SERVER["REQUEST_METHOD"] == 'POST'){
                echo('is-valid');
            }
            ?>" type="password" name="confirm_password">

            <?php
            if(array_key_exists("confirm_password", $errors)){
                echo(' <div class="invalid-feedback">
                '.$errors["confirm_password"].'
            </div>');
            }
            ?>
        </div>

        <input type="submit"  class="btn btn-success mt-2">

    </form>
</div>
<?php
include 'parts/scripts.php'
?>
</body>
</html>