<html>
<head>
    <?php
    include 'blocks/stylesheets.php';
    ?>
</head>
<body>
<div class="container">

    <?php
        // Je déclare un tableau qui stockera toutes mes erreurs
        $errors = [];

        // Je vérifie mes erreurs seulement quand je suis sur une méthode post
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            // Si jamais je n'ai rien saisi dans le champ username
            if(empty($_POST["username"])){
                // J'ajoute dans mon tableau d'erreur une erreur sur le champs username
                $errors["username"] = "Veuillez saisir un username";
            }

            // Je vérifie que mon username fait au moins 3 caractères
            if(strlen($_POST["username"])<=3){
                // J'ajoute dans mon tableau d'erreur une erreur sur le champs username
                $errors["username"] = "Votre nom d'utilisateur doit faire au moins 3 caractères";
            }
        }
    ?>
    <?php
    include 'blocks/header.php';
    include 'blocks/menu.php';
    include_once 'blocks/menu.php';
    ?>
    <h1>Créer un compte !</h1>

    <form method="post">
        <div class="form-group">
            <label>Username</label>
            <input type="text" value="<?php
            if(!empty($_POST["username"])){
                echo($_POST["username"]);
            } ?>" class="form form-control <?php
                if(array_key_exists("username", $errors)){
                    echo('is-invalid');
                }
            ?>" name="username">

            <div class="invalid-feedback">
               <?php
               if(array_key_exists("username", $errors)){
                   echo($errors["username"]);
               }
               ?>
            </div>
        </div>

        <input type="submit" class="btn btn-success mt-3">
    </form>



    <?php
    include 'blocks/footer.php';
    ?>
</div>

<?php
include "blocks/javascripts.php";
?>
</body>
</html>