<?php
require "loader.php";


if(empty($_GET)){
    header("Location: index.php?controller=techno&action=list");
}

if($_GET["controller"] == 'techno'){
    $controller = new TechnosController();
    if($_GET["action"] == "list"){
        $controller->getAllTechnos();
    }
    if($_GET["action"] == "detail" && array_key_exists("id", $_GET)){
        $controller->detail($_GET["id"]);
    }
    if($_GET["action"] == "delete" && array_key_exists("id", $_GET)){
        $controller->remove($_GET["id"]);
    }
    if($_GET["action"] == 'add'){
        $controller->add();
    }
    if($_GET["action"] == "edit" && array_key_exists("id", $_GET)){
        $controller->edit($_GET["id"]);
    }

}

if($_GET["controller"] == "security"){
    $controller = new SecurityController();

    if($_GET["action"] == "register"){
        $controller->register();
    }

    if($_GET["action"] == "login"){
        $controller->login();
    }

    if($_GET["action"] == 'logout'){
        $controller->logout();
    }
}