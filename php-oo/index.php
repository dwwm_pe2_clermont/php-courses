<?php
    class Pokemon {
        private $type;
        private $name;
        private $pv;

        public function __construct($type, $name, $pv){
           $this->type = $type;
           $this->name = $name;
           $this->pv = $pv;
        }

        public function getType(){

           return $this->type;
        }

        public function setType($type){
            $this->type  = $type;
        }
    }

    $pokemon = new Pokemon("Electric", "Pikachu", 100);
    var_dump($pokemon);
    $pokemon->setType("Feu");
    var_dump($pokemon);


    $pokemon2 = new Pokemon("Feu", "Salamèche", 20);
    var_dump($pokemon2);

?>