<?php
// Ici j'irais effectuer toutes mes requêtes SQL et je retournerais des objet
class UserManager extends DbManager {

    public function add(User $user)
    {
        $query = $this->bdd->prepare(
            "INSERT INTO user (email, password) VALUES (:email, :password)"
        );

        $query->execute([
            "email"=> $user->getEmail(),
            "password"=> $user->getPassword()
        ]);
    }

    public function findByEmail($email){
        $user = null;
        $query = $this->bdd->prepare("SELECT * FROM user WHERE email = :email");
        $query->execute([
            "email"=> $email
        ]);

        $ressult = $query->fetch();

        if($ressult){
            $user = new User($ressult["id"], $ressult["email"], $ressult["password"]);
        }

        return $user;
    }
}