<?php
    // Classe qui représente le compte en banque
    class CompteBancaire{
        // Cet attribut représente le solde de mon compte
        private $solde;

        private $decouvertAutorise;

        private $plafond;


        // Quand j'ouvre un compte, je mets de l'argent dessus
        // J'utilise le constructeur pour initialiser le solde de mon compte
        public function __construct($solde, $decouvertAutorise, $plafond){
            $this->solde = $solde;
            $this->decouvertAutorise = $decouvertAutorise;
            $this->plafond = $plafond;
        }

        public function __destruct(){
            var_dump('Mon objet est détruit !');
        }


        // Cette méthode nous permet d'afficher le solde du compte
        // en dehors de mon objet
        public function getSolde(){
            return $this->solde;
        }

        // Cette méthode me permet d'encaisser un paiement
        public function encaisser($montant){
            // Mon attribut solde sera donc additionné au montant du paiement
            $this->solde = $this->solde + $montant;
        }

        // Cette méthode me permet de payer
        public function pay($montant){
            // Si le montant du paiement est supérieur à mon solde
            // Si le montant est supérieur au solde et que je n'ai
            // pas de découvert autorisé, c'est un refus
            if(($montant > $this->solde && !$this->decouvertAutorise)){
                echo('Refus de paiement !<br>');
            } else {
                // Je vérifie que mon plafond est respecté
                if(($this->solde - $montant) >= -$this->plafond){
                    // Si mon plafond est respecté, je modifie le solde
                    $this->solde = $this->solde-$montant;
                } else {
                    // Refus de paiement a cause de mon plafond
                    echo('Refus de paiement !<br>');
                }
            }

        }
    }

    $compteMathieu = new CompteBancaire(100000, true, 200);
    $compteMathieu->pay(200000);

    $compteAurelien = new CompteBancaire(10, false, null);
    $compteAurelien->pay(5);
    var_dump($compteAurelien->getSolde());

    unset($compteAurelien);
    var_dump("toto");

/*    // Aurélien a ouvert un compte bancaire et a mis 10 euros dessus
    $compteAurelien = new CompteBancaire(10, false, null);
    // Mathieu a ouvert un compte bancaire avec 1000000 euros
    $compteMathieu = new CompteBancaire(100000, true, 200);

    // J'affiche le solde du compte de aurélien via la méthode publique
    // getSolde
    echo('Le compte de Aurélien a '. $compteAurelien->getSolde().' euros <br>');
    // J'affiche le solde du compte de mathieu via la méthode publique
    // getSolde
    echo('Le compte de Mathieu a '. $compteMathieu->getSolde().' euros<br>');

    // Aurélien ajoute 200 euros sur son compte
    $compteAurelien->encaisser(200);
    echo('Le compte de Aurélien a '. $compteAurelien->getSolde().' euros <br>');
    echo('Le compte de Mathieu a '. $compteMathieu->getSolde().' euros<br>');

    // Matthieu paye 2000 euros avec son compte
    $compteMathieu->pay(2000);
    // Je raffiche les soldes des comptes de Mathieu et Aurélien
    echo('Le compte de Aurélien a '. $compteAurelien->getSolde().' euros <br>');
    echo('Le compte de Mathieu a '. $compteMathieu->getSolde().' euros<br>');

    // Aurélien paye 300 euros
    $compteAurelien->pay(300);
    // Je raffiche le nouveau solde de Aurélien
    echo('Le compte de Aurélien a '. $compteAurelien->getSolde().' euros <br>');


    $compteMathieu->pay(10);
    echo('Le compte de Mathieu a '.$compteMathieu->getSolde().' euros<br>');

    $compteAurelien->pay(211);
    echo('Le compte de Aurélien a '. $compteAurelien->getSolde().' euros <br>');*/

?>