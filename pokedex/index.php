<?php
    // Tableau qui représente tous les pokemons affichés dans mon slider
    $sliders = [
            [
                    "src"=> "https://cdn.shopify.com/s/files/1/2500/4214/files/pokemon-pikachu-asleep-i101272.jpg?v=1623085760",
                    "titre"=> "Pikachu",
                    "description"=> "Pokemon de type foudre",
                    "actif"=> false
            ],
            [
                "src"=> "https://www.realite-virtuelle.com/wp-content/uploads/2022/11/charmander-758x426.jpg",
                "titre"=> "Salamèche",
                "description"=> "Pokemon de type feu",
                "actif"=> true
            ],
            [
                "src"=> "https://www.pokepedia.fr/images/thumb/0/09/Bulbizarre_de_Sacha.png/800px-Bulbizarre_de_Sacha.png",
                "titre"=> "Bulbizar",
                "description"=> "Pokemon de type herbe",
                "actif"=> false
            ]
    ]
?>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Pockedex !</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Acceuil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Mes pokemon</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <?php
        $i=0;
        foreach ($sliders as $slider){
            $actif = ($slider["actif"])?"active":"";
            echo('  <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="'.$i.'" class="'.$actif.'" aria-current="true" ></button>');
            $i++;
        }
        ?>
    </div>
    <div class="carousel-inner">
        <?php
            // Je parcours ce tableau
            foreach ($sliders as $slider){
                $actif = ($slider["actif"])?"active":"";
               echo('<div class="carousel-item '.$actif.'">
            <img src="'.$slider["src"].'" class="d-block w-100" alt="...">
            <div class="carousel-caption d-md-block">
                <h5>'.$slider["titre"].'</h5>
                <p>'.$slider["description"].'</p>
            </div>
        </div>');
            }
        ?>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>