<?php
var_dump($_GET);
var_dump($_POST);
?>

<html>
<head>

</head>
<body>
<h1>Requête GET</h1>
<form>

    <input name="search"
           value="<?php
           if(array_key_exists("search", $_GET)){echo($_GET['search']);}?>">
    <input type="submit">
</form>

    <h1>Requête POST</h1>
    <form method="post">
        <div>
            <label>Email</label>
            <input type="text" name="email" value="<?php
            if(array_key_exists("email", $_POST)){echo($_POST["email"]);}?>">

            <div>
                <?php
                if(array_key_exists("email", $_POST)) {
                    if(empty($_POST["email"])){
                        echo('Veuillez saisir un email');
                    }
                    elseif (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                        echo($_POST["email"] . " is a valid email address");
                    } else {
                        echo($_POST["email"] . " is not a valid email address");
                    }
                }
                ?>
            </div>
        </div>

        <div>
            <label>Mot de passe</label>
            <input type="password" name="password">
            <div>
                <?php
                if(array_key_exists("password", $_POST)) {
                    $motDePasse = $_POST["password"];
                    $pattern = '/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/';

                    if(preg_match($pattern, $motDePasse)){
                        echo('Mot de passe valide !');
                    } else {
                        echo("Le mot de passe doit contenir 8 caractères, et un chiffre !");
                    }

                }
                ?>
            </div>
        </div>
        <input type="submit">
    </form>
</form>
</body>
</html>
