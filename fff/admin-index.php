<?php
    // session_start()
    require 'functions.php';
    restrictAccess();
    $pdo = dbConnect();
    $query = $pdo->query("SELECT * FROM player");
    $resultats = $query->fetchAll();
?>
<html>
<head>
    <?php
        include 'parts/stylesheets.php';
    ?>
</head>
<body>

<div class="container">
    <?php
        require "parts/menu.php"
    ?>

    <table class="table">
        <thead>
            <th>#</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Date de naissance</th>
            <th>Poste</th>
            <th>Action</th>
        </thead>
        <tbody>
        <?php
            foreach ($resultats as $resultat){
                $date = new \DateTime($resultat["date_naissance"]);

                echo('<tr>
                    <td>'.$resultat["id"].'</td>
                      <td>'.htmlentities($resultat["nom"]).'</td>
                       <td>'.htmlentities($resultat["prenom"]).'</td>
                       <td>'.$date->format("d/m/Y").'</td>
                         <td>'.$resultat["poste"].'</td>
                           <td>
                           <a href="admin-edit.php?id='.$resultat["id"].'">Editer</a>
                           <a href="admin-delete.php?id='.$resultat["id"].'">Supprimer</a>
                            </td>
                </tr>');
            }
        ?>
        </tbody>
    </table>
    <?php
        if(count($resultats)<23){
            echo(' <a href="admin-joueur-add.php" class="btn btn-success mt-3">Ajouter un joueur</a>');
        }
    ?>

</div>

<?php
include 'parts/javascripts.php';
?>
</body>
</html>