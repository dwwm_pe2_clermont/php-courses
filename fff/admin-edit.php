<?php
// session_start();
require 'functions.php';
restrictAccess();
$errors = [];

$pdo = dbConnect();

$query = $pdo->prepare("SELECT * FROM player WHERE id = :id");
$query->execute(["id"=> $_GET["id"]]);
$playerEdit = $query->fetch();

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty($_POST["nom"])){
        $errors["nom"] = "Veuillez saisir un nom";
    }
    if(empty($_POST["prenom"])){
        $errors["prenom"] = "Veuillez saisir un prénom";
    }
    if(empty($_POST["date_naissance"])){
        $errors["date_naissance"] = "Veuillez saisir une date de naissance";
    }
    if(empty($_POST["poste"])){
        $errors["poste"] = "Veuillez saisir un poste";
    }

    if(strtotime($_POST["date_naissance"]) == false){
        $errors["date_naissance"] = "Le format de la date est invalide !";
    }

    if(count($errors) == 0){


        $request = $pdo->prepare(
            "UPDATE player SET nom = :nom, prenom = :prenom, date_naissance = :date_naissance, poste = :poste WHERE id = :id");
        $request->execute([
            "nom"=> $_POST["nom"],
            "prenom"=> $_POST["prenom"],
            "date_naissance"=> $_POST["date_naissance"],
            "poste"=> $_POST["poste"],
            "id"=> $_GET["id"]
        ]);

        header('Location: admin-index.php');
    }
}
?>
<html>
<head>
    <?php
    include 'parts/stylesheets.php';
    ?>
</head>
<body>

<div class="container">
    <?php
    require "parts/menu.php"
    ?>

    <a href="admin-index.php" class="btn btn-warning mt-3 mb-3">Retour</a>

    <form method="post">
        <div class="form-group">
            <label>Nom</label>
            <input type="text" name="nom" placeholder="nom du joueur"
                   value="<?php keepFormValue("nom", $playerEdit); ?>"
                   class="form-control <?php displayBsClassForm($errors, 'nom');?>">

            <?php displayBsErrorForm($errors, 'nom'); ?>
        </div>

        <div class="form-group">
            <label>Prénom</label>
            <input type="text"
                   value="<?php keepFormValue("prenom", $playerEdit); ?>"
                   name="prenom" placeholder="prénom du joueur"
                   class="form-control <?php displayBsClassForm($errors, 'prenom');?>">
            <?php displayBsErrorForm($errors, 'prenom'); ?>
        </div>

        <div class="form-group">
            <label>Date de naissance</label>
            <input type="date"
                   value="<?php keepFormValue("date_naissance", $playerEdit); ?>"
                   name="date_naissance" placeholder="Date de naissance du joueur"
                   class="form-control <?php displayBsClassForm($errors, 'date_naissance');?>">
            <?php displayBsErrorForm($errors, 'date_naissance'); ?>
        </div>

        <div class="form-group">
            <label>Poste</label>
            <select name="poste" class="form-select <?php displayBsClassForm($errors, 'poste');?>">
                <option></option>
                <option <?php
                if(($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["poste"] == 'gardien') ||
                    ($_SERVER["REQUEST_METHOD"] == "GET" && $playerEdit["poste"] == 'gardien')
                ){echo('selected');}?> value="gardien">Gardien</option>
                <option <?php if(($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["poste"] == 'defenseur')||
                    ($_SERVER["REQUEST_METHOD"] == "GET" && $playerEdit["poste"] == 'defenseur')){echo('selected');}?> value="defenseur">Défenseur</option>
                <option <?php if(($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["poste"] == 'milieu') ||
                    ($_SERVER["REQUEST_METHOD"] == "GET" && $playerEdit["poste"] == 'milieu')){echo('selected');}?> value="milieu">Milieu</option>
                <option <?php if(($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["poste"] == 'attaquant')||
                    ($_SERVER["REQUEST_METHOD"] == "GET" && $playerEdit["poste"] == 'attaquant')){echo('selected');}?> value="attaquant">Attaquant</option>
            </select>

            <?php displayBsErrorForm($errors, 'poste'); ?>
        </div>

        <input type="submit" class="btn btn-success mt-3">
    </form>


</div>

<?php
include 'parts/javascripts.php';
?>
</body>
</html>