<?php
class Car{

    public static $allowedEnergy = ["essence", "diesel",
                                    "électrique", "hybrid"];

    private $id;
    private $marque;
    private $model;
    private $energy;
    private $automatic;
    private $picture;

    public function __construct($id, $marque, $model,
                                $energy, $automatic, $picture){
        $this->id = $id;
        $this->marque = $marque;
        $this->model = $model;
        $this->energy = $energy;
        $this->automatic = $automatic;
        $this->picture = $picture;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getMarque(){
        return htmlentities($this->marque);
    }

    public function getModel()
    {
        return htmlentities($this->model);
    }

    public function setModel($model): void
    {
        $this->model = $model;
    }

    public function getEnergy()
    {
        return $this->energy;
    }

    public function setEnergy($energy): void
    {
        $this->energy = $energy;
    }

    public function getAutomatic()
    {
        return $this->automatic;
    }

    public function setAutomatic($automatic): void
    {
        $this->automatic = $automatic;
    }

    public function getPicture()
    {
        return htmlspecialchars($this->picture);
    }

    public function setPicture($picture): void
    {
        $this->picture = $picture;
    }


}