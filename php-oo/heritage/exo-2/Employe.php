<?php
    class Employe extends Personne {
        protected $salaire;

        public function __construct($nom, $prenom, $age, $salaire){
            parent::__construct($nom, $prenom, $age);
            $this->salaire = $salaire;
        }

        public function getSalaire(){
            return $this->salaire;
        }

        public function setSalaire($salaire){
            $this->salaire = $salaire;
        }

        public function getAge(){
            return parent::getAge();
        }


    }
?>