<html>

<head>

</head>

<body>
<?php

$errors = [];
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (empty($_POST["username"])) {
        $errors["username"] = "Veuillez saisir un username";
    }
}
?>
<main>

    <h1>Créer un compte</h1>
    <form method="post">
        <div class="form-group">
            <label>Username</label>
            <input type="text" value="<?php if (!empty($_POST["username"])) {
                echo ($_POST["username"]);
            } ?>" class="form form-control <?php
            if (array_key_exists("username", $errors)) {
                echo ('is-invalid');
            } ?>" name="username">


        </div>
        <input type="submit">
    </form>
</main>
</body>

</html>