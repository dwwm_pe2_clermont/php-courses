<?php
    class Calculette {
        public static function additionner($nombre1, $nombre2){
            return $nombre1 + $nombre2;
        }

        public static function soustraction($nombre1, $nombre2){
            return $nombre1 - $nombre2;
        }

        public static function multiplier($nombre1, $nombre2){
            return $nombre1 * $nombre2;
        }

        public static function diviser($nombre1, $nombre2){
            return $nombre1/$nombre2;
        }
    }
?>