# Cours de PHP

## Lundi 13 Novembre

### Matin 

#### Configuration PHP 

Il existe différentes configuration de PHP en fonction de ce que l'on souhaite faire 

- Une application en développement qui affichera les erreurs 
- Une application en production qui sera plus silencieuse pour ne pas donner d'informations à un potentiel hackeur

Pour afficher la configuration de notre PHP, il faut : 

Dans un fichier ayant l'extension php (.php), utiliser la fonction phpinfo();


```php
<?php
    phpinfo();
?>
```

Cette configuration est localisée dans le fichier php.ini. Configuration globale. 

La méthode ini_set permet de modifier la configuration pendant la durée du script en cours
La méthode ini_get permet de réccupérer la valeur de la configuration

```php
// Ici pendant la durée du script, les erreurs ne seront plus affichées
ini_set('display_errors', O);
// Ici, je réccupére 0 puisque la valeur de la configuration display_errors est maintenant égale à 0
ini_get('display_errors');
```


#### Les commentaires

En PHP nous avons 2 facons de faire des commentaires : 

- Les commentaires sur une seule ligne
- Les commentaires sur plusieurs lignes
```php
 // Les commentaires sur une seule ligne 

/*
 Les 
 commentaires 
 sur 
 plusieurs 
 lignes
 */
```
- Déclarer une variable en php

Un utilise systématiquement un $ dans la déclaration d'une variable. Ensuite on indique le nom de la variable, le signe égale et la valeur de la variable.

On a plusieurs types de variables : 

- Chaine de caractères ('Aurélien'),
- Booléen (true / false)
- Float (1.89)
- Entier (12)

```php
// Déclaration d'une variable de type integer
$variableInt = 10;
// Déclaration d'une variable de type booléen
$variableBool = true;
// Déclaration d'une variable de type float
$variableFloat = 12.35;
// Déclaration d'une variable de type chaine de caractère
$variableChar1 = "Hello world";
$variableChar2 = 'Hello world';

```


#### Les chaines de caractères : 

Les valeurs d'une chaine de caractères peuvent être délimitées par un guillement ou une quote.

Le backslash permettra d'échaper un caractère.

Exemple : Une chaine de caractère qui contient un slash mais qui est aussi délimitée par un /

```php
echo('Je m\'appel Aurélien');
```

On peut concaténer plusieurs variables de type chaine de caractère en utilisant un .

```php
$firstname = 'Aurélien';
$lastname = 'Delorme';

// Affichera Aurélien Delorme
echo($firstname.' '.$lastname);
```

La méthode die(); 

Permet de stopper l'execution du script php

```php
// Hello sera affiché à l'écran
echo('Hello');
die();
echo('World');
```


#### Les conditions : 

Permet d'effectuer des actions en fonction de la valeur d'une variable 

```php
$valeur = true;

if($valeur){
 // Action si vrai
} else {
 // Action si faux
}
```

#### Les calculs :

```php
$variable1 = 2;
$variable2 = 10;

// Faire une addition : 
// $resultat sera égal à 12
$resultat = $variable1+$variable2;

// Faire une soustraction 
// $resultat sera égal à 8
$resultat = $variable2-$variable1;

// Faire une division 
// $resultat sera égal à 5
$resultat = $variable2/$variable1;

// Faire une multiplication 
// $resultat sera égal à 20
$resultat = $variable1*$variable2

// Faire un modulo (reste de la division)
// Ici $result sera égal à 0 
$resultat = $variable2%$variable1;
```
## Mardi 14 Novembre

### Matin 

Conditions
- Opérateurs de comparaison
    - > supérieur à ">"
    - > supérieur ou égal ">="
    - > inférieur "<"
    - > inférieur ou égal "<="
    - > différent de "!="
    - > égal à "==" (vérifie la valeur et non le type)
    - > égal à "===" (vérifie la valeur et le type)

- Conditions multiples
    - Utilisation de la porte logique ET

```php
    // Utilisation du mot clé AND
    if($a AND $b){
        // Si a est vrai et b est vrai
        // je passe ici ! 
    }
    // Utilisation de ||
    if($a && $b){
        // Si a est vrai et b est vrai
        // je passe ici ! 
    }
````
    - Utilisation de la porte logique OU

```php

if($a OR $b){
 // Je passe ici si a est vrai OU b est vrai
}


if($a || $b){
 // Je passe ici si a est vrai OU b est vrai
}
```

- Switch : 
Permet de combiner plusieurs conditions. Le mot clé break permet de sortir du switch lorsqu'une action est réalisée

```php
$variable = 10;

switch ($variable) {
    case $variable>16 : 
        echo('Mention très bien');
        break;
    case $variable>14 : 
        echo('Mention bien');
        break;
    case $variable>10 : 
        echo('Tu as a moyenne');
        break;
    default :
        echo('Rattrapage !')    
}
```

- Ternaires

Permet de condenser une condition avec affectation de valeur

````php
// Si l'age est supérieur ou égal à 18, 
// le booléen isMajeur sera égal à true
// Sinon il sera égale à false
$isMajeur = ($age>=18)?true:false
````

Tableaux
- Tableaux numérotés
Permet de faire une liste (pile d'élément). Par exemple une liste de course, une todo list, ...
Ils seront retrouvés en fonction d'un index représenté par leur ordre dans la pile. Le premier élément de la pile aura l'index 0

```php
// La déclaration d'un tableau vide
$array = []; 

// Déclaration d'un tableau avec deux éléments dans la pile
$array = ["element 1", "element 2"];

// Ajoute element 3 dans le tableau
$array[] = "element 3";
```

- Tableaux associatifs
Représente un tableau avec des clés et des valeurs

```php
$array = [
    "nom"=> "Delorme"
    "prenom"=> "Aurélien"
];

// Affichera : Aurélien
echo($array["prenom"]);

```

Boucles

- for
Permet d'effectuer une action en fonction d'un nombre d'occurence
Exemple : Afficher les nombres de 1 à 10

```php
for($i=0;$i<10;$i++){
    echo($i);
}
```

- foreach
Prend en paramètre un tableau, le mot clé "as" et un alias
Il parcourt tous les éléments du tableau

```php

// Dans le cas d'un tableau numéroté
$fruits = ["Pomme", "Poire"];

// Affichera "PommePoire"
foreach ($fruits as $fruit){
    echo($fruit);
}

// Dans le cas d'un tableau clé => valeur
$identity = [
    "prenom"=>"toto",
    "nom"=> "tata"
];

// Affichera prenomnom
foreach ($identity as $key=>$value){
    echo($key);
}

// Affichera tototata
foreach ($identity as $key=>$value){
    echo($value);
}

// Si je veux afficher une seule valeur
echo($identity["prenom"]);

```
- While
Permet d'effectuer une action tant qu'une condition est valable

```php
$variable = true;
// Affichera je suis vrai
while ($variable == true){
    echo('Je suis vrai');
    // La condition n'est plus vérifiée donc on va sortir !
    $variable = false;
}
```
-0n a pas laché l'affaire avec XDebug