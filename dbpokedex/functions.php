<?php
// Fonction qui se connecte à la base de données
function dbConnect(){
    // Nous nous connectons à notre base de données
    try {
        $host = 'database';
        $dbName = 'pokedex';
        $user = 'root';
        $password = 'tiger';

        $pdo = new PDO(
            'mysql:host='.$host.';dbname='.$dbName.';charset=utf8',
            $user,
            $password);
        // Cette ligne demandera à pdo de renvoyer les erreurs SQL si il y en a
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Je retourne ma connexion
        return $pdo;
    }
    catch (PDOException $e) {
        throw new InvalidArgumentException('Erreur connexion à la base de données : '.$e->getMessage());
        exit;
    }
}

function displayValidationBootstrapClass($errors, $inputName){
    if($_SERVER["REQUEST_METHOD"] == 'POST') {
        if (array_key_exists($inputName, $errors)) {
            echo('is-invalid');
        } else {
            echo('is-valid');
        }
    }
}

function displayFormValue($inputName, $result = null){
    if($_SERVER["REQUEST_METHOD"] == 'POST') {
        echo($_POST[$inputName]);
    }
    elseif(!is_null($result)){
        echo($result[$inputName]);
    }
}

function displayFormError($errors, $inputName){
    if($_SERVER["REQUEST_METHOD"] == 'POST') {
        if(array_key_exists($inputName, $errors)){
            echo(' <div id="validationServerUsernameFeedback" class="invalid-feedback">
               '.$errors[$inputName].'
                </div>');
        }
    }
}

function validateForm(){
    $errors = [];
    if(empty($_POST["nom"])){
        $errors["nom"] = "Veuillez saisir un nom";
    }

    if(empty($_POST["pv"])){
        $errors["pv"] = "Veuillez saisir le nombre de point de vie";
    } elseif(!is_numeric($_POST["pv"])){
        $errors["pv"] = 'Veuillez saisir un nombre entier';
    } elseif ($_POST["pv"] <= 0 || $_POST["pv"] > 200){
        $errors["pv"] = "Le nombre de point de vie doit être compris entre 0 et 200";
    }

    if(empty($_POST["attaque"])){
        $errors["attaque"] = "Veuillez saisir le nombre de point d'attaque";
    } elseif(!is_numeric($_POST["attaque"])){
        $errors["attaque"] = 'Veuillez saisir un nombre entier';
    } elseif ($_POST["attaque"] <= 0 || $_POST["attaque"] > 200){
        $errors["attaque"] = "Le nombre de point d'attaque doit être compris entre 0 et 200";
    }

    if(empty($_POST["defense"])){
        $errors["defense"] = "Veuillez saisir le nombre de défense";
    } elseif(!is_numeric($_POST["defense"])){
        $errors["defense"] = 'Veuillez saisir un nombre entier';
    } elseif ($_POST["defense"] <= 0 || $_POST["defense"] > 200){
        $errors["defense"] = "Le nombre de point de défense doit être compris entre 0 et 200";
    }

    if(empty($_POST["vitesse"])){
        $errors["vitesse"] = "Veuillez saisir le nombre de vitesse";
    } elseif(!is_numeric($_POST["vitesse"])){
        $errors["vitesse"] = 'Veuillez saisir un nombre entier';
    } elseif ($_POST["vitesse"] <= 0 || $_POST["vitesse"] > 200){
        $errors["vitesse"] = "Le nombre de point de vitesse doit être compris entre 0 et 200";
    }

    if(empty($_POST["special"])){
        $errors["special"] = "Veuillez saisir le nombre de spécial";
    } elseif(!is_numeric($_POST["special"])){
        $errors["special"] = 'Veuillez saisir un nombre entier';
    } elseif ($_POST["special"] <= 0 || $_POST["special"] > 200){
        $errors["special"] = "Le nombre de point de spécial doit être compris entre 0 et 200";
    }

    return $errors;
}

function testUpload(){
    $allowedExtension = ["image/jpeg", "image/png"];

    $error = null;

    if(!in_array($_FILES["image"]["type"], $allowedExtension)){
        $error = "Nous n'acceptons pas ce type de fichier (veuillez uploader une image)";
    }

    if($_FILES["image"]["error"] != 0){
        $error = "Une erreur s'est produite lors de l'upload";
    }

    if($_FILES["image"]["size"]>2097152){
        $error = "Le fichier est trop lourd ! Maximum 2Mo";
    }

    return $error;
}

function checkConnection(){
    if(!array_key_exists("user", $_SESSION)){
        header("Location: login.php");
    }
}