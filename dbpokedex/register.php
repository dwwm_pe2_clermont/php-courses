<?php
require "functions.php";

$errors = [];

if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty($_POST["lastname"])){
        $errors["lastname"] = "Veuillez saisir votre nom";
    }

    if(empty($_POST["firstname"])){
        $errors["firstname"] = "Veuillez saisir votre prénom";
    }


    if(empty($_POST["email"])){
        $errors["email"] = "Veuillez saisir un email";
    } elseif(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
        $errors["email"] = "Votre email n'est pas valide";
    }

    if(empty($_POST["password"])){
        $errors["password"] = "Veuillez saisir votre mot de passe";
    }

    if($_POST["password"] != $_POST["confirm_password"]){
        $errors["confirm_password"] = "Les mots de passes ne sont pas identiques";
    }

    if(count($errors) == 0){
        $pdo = dbConnect();

        $request = $pdo->prepare("INSERT INTO users (firstname, lastname, email, password)
                            VALUES (:firstname, :lastname, :email, :password)");
        $request->execute([
                "firstname"=> $_POST["firstname"],
                "lastname"=> $_POST["lastname"],
                "email"=> $_POST["email"],
                "password"=> password_hash($_POST["password"], PASSWORD_DEFAULT)
        ]);

        header("Location: login.php");
    }
}
?>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>Créer un compte</h1>
    <form method="post">
        <div class="form-control">
            <label>Nom de l'utilisateur</label>
            <input class="form-control <?php displayValidationBootstrapClass($errors, 'lastname');?>"
                   value="<?php displayFormValue('lastname'); ?>"
                   type="text" name="lastname" placeholder="Nom">
            <?php displayFormError($errors, "lastname"); ?>
        </div>

        <div class="form-control">
            <label>Prénom de l'utilisateur</label>
            <input class="form-control <?php displayValidationBootstrapClass($errors, 'firstname');?>"
                value="<?php displayFormValue('firstname'); ?>"
                   type="text" name="firstname" placeholder="Prénom">
            <?php displayFormError($errors, "firstname"); ?>
        </div>

        <div class="form-control">
            <label>Email de l'utilisateur</label>
            <input class="form-control <?php displayValidationBootstrapClass($errors, 'email');?>"
                   value="<?php displayFormValue('email'); ?>"
                   type="text" name="email" placeholder="Email">
            <?php displayFormError($errors, "email"); ?>
        </div>

        <div class="form-control">
            <label>Mot de passe de l'utilisateur</label>
            <input class="form-control <?php displayValidationBootstrapClass($errors, 'password');?>"
                   type="password" name="password" placeholder="Mot de passe">
            <?php displayFormError($errors, "password"); ?>
        </div>

        <div class="form-control">
            <label>Confirmation</label>
            <input class="form-control <?php displayValidationBootstrapClass($errors, 'confirm_password');?>"
                   type="password" name="confirm_password" placeholder="Confirmez">
            <?php displayFormError($errors, "confirm_password"); ?>
        </div>

        <input type="submit" class="btn btn-success mt-3">
    </form>

    <a href="login.php">J'ai déjà un compte !</a>
</div>
</body>
