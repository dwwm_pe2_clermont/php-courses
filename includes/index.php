<html>
<head>
    <?php
        include 'blocks/stylesheets.php';
    ?>
</head>
<body>
    <div class="container">

        <?php
            include 'blocks/header.php';
            include 'blocks/menu.php';
            include_once 'blocks/menu.php';
        ?>
    <h1>Welcome !</h1>

    <div>
        Ma page d'acceuil
    </div>

        <h2>Toutes les prestations</h2>
        <div class="row">
        <?php
            $prestations = [
                    [
                        "id"=> 1,
                        "nom"=> "Tache de ketchup",
                        "description"=> "Nous enlevons le ketchup"
                    ],
                    [
                            "id"=>2,
                            "nom"=> "Tache de moutarde",
                            "description"=> "Moutarde de Dijon"
                    ]
            ];

            foreach ($prestations as $prestation){
                echo('<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">'.$prestation["nom"].'</h5>
    <h6 class="card-subtitle mb-2 text-muted">'.$prestation["description"].'</h6>
    <a href="product.php?nom='.$prestation["nom"].'&description='.$prestation["description"].'" class="card-link">Voir le détail de la prestation</a>
  
  </div>
</div>');
            }
        ?>
        </div>


        <?php
        include 'blocks/footer.php';
        ?>
    </div>

    <?php
        include "blocks/javascripts.php";
    ?>
</body>
</html>