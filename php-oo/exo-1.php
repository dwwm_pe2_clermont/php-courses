<?php
// Classe qui représente une formation
class Formation {
    // Attribut qui représente la date de début de la formation
    private $dateDebut;
    // Attribut qui représente la date de fin de la formation
    private $dateFin;
    // Attribut qui représente le thème de la formation
    private $theme;

    // Ici nous avons un constructeur
    // Cette méthode est une méthode magique qui est appelée implicitement lorsque
    // l'on utilise le mot clé new
    // Il prend 3 paramètres qui permettent d'initialiser les 3 attributs ci-dessus
    public function __construct($dateDebut, $dateFin, $theme)
    {
        // Si la date de début est plus grande que la date de fin, j'affiche un message
        if($dateDebut>$dateFin){
            echo('Impossible ! La date de début est avant la date de fin !');
        } else {
            // J'affecte le paramètre 1 à l'attribut dateDebut de mon objet
            $this->dateDebut = $dateDebut;
            // J'affecte le paramètre 2 à l'attribut dateFin de mon objet
            $this->dateFin = $dateFin;
            // J'affecte le paramètre 3 à l'attribut theme de mon objet.
            $this->theme = $theme;
        }

    }

    // Notre attribut date début est privé. Je n'ai pas d'accès dessus
    // En revanche, pour réccupérer sa valeur, je pourrais passer par cette
    // méthode qui est publique et qui retourne la valeur de mon attribut
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    // La méthode setDateDebut permet de mettre à jour l'attribut dateDebut qui est privé
    // Elle est publique et sera donc accessible en dehors de l'objet.
    public function setDateDebut($dateDebut)
    {
        if($dateDebut > $this->dateFin){
            echo('Modification impossible, la date de début est plus grande
            que la date de fin !');
        } else {
            $this->dateDebut = $dateDebut;
        }

    }

    // Notre attribut date début est privé. Je n'ai pas d'accès dessus
    // En revanche, pour réccupérer sa valeur, je pourrais passer par cette
    // méthode qui est publique et qui retourne la valeur de mon attribut
    public function getDateFin()
    {
        return $this->dateFin;
    }

    // La méthode setDateDebut permet de mettre à jour l'attribut dateDebut qui est privé
    // Elle est publique et sera donc accessible en dehors de l'objet.
    public function setDateFin($dateFin)
    {
        if($dateFin<$this->dateDebut){
            echo('Impossible la date de fin est avant la date de début !');
        } else {
            $this->dateFin = $dateFin;
        }

    }

    public function getTheme()
    {
        return $this->theme;
    }

    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    // Méthode qui retourne toutes les informations de mon objet dans
    // un tableau
    public function getAllInfos(){
       return [
           "dateDebut" => $this->dateDebut,
           "dateFin" => $this->dateFin,
           "theme"=> $this->theme
       ];
    }
}

// Je cré un objet contenant une date de début.
$dateDebut = new \DateTime("1993-03-30");
// Je cré un nouvel attribut qui représente la date de fin
$dateFin = new \DateTime("now");

// Je cré un nouvel objet de type Formation prenant 3 paramètres
// puisque j'ai 3 paramètres dans mon constructeur
$formation = new Formation($dateDebut, $dateFin, "DEVWEB");
// Je modifie la date de début
$formation->setDateDebut(new DateTime("2019-02-10"));

var_dump($formation->getAllInfos());
// J'affiche mon objet Formation.
//var_dump($formation);