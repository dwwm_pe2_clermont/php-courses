# **Récap 07/12**

## Classe / Objet / Attributs / Méthodes

On utilise la POO pour une meilleur organisation de notre code source. Notre code sera réutilisable. Cela évite la duplication de code. Notre application sera donc plus évolutive et plus facile à maintenir

Un objet contient : 
- Des attributs (les caractèristiques de l'objet) (taille de l'objet, couleur, ...)
- Des méthodes (les fonctionnalités de l'objet) (calculer, additionner, multiplier, ...)

Une classe est un moule qui nous permet d'usiner un objet. 
On cré un nouvel objet grâce au mot clé new. Ce mot clé utilise notre classe pour usiner un nouvel objet.

Création d'une classe qui s'appelle "MaClasse"
````php
<?php
    class MaClasse{
        private $myAttribut;
        public function maFonction(){
            echo('Body de ma fonction')
        }
    }
````

Instanciation d'un nouvel objet de type MaClasse : 

````php

    $object = new MaClasse();

````
## Visibilité des attributs et des méthodes

- public : Un attribut ou une méthode publique est accessible dans tout notre programme une fois l'objet créé
- private : Un attribut ou une méthode private est accessible seulement au sein de notre objet
- protected : Un attribut ou une méthode protected est accessible dans la classe ou dans une classe qui hérite de la classe ou est déclaré l'attribut ou la méthode. 

## Méthodes magiques

Les méthodes magiques se reconnaissent car elle commencent par '__'
Les méthodes dites magiques sont des méthodes qui sont appelées implicitement (on a pas besoin de les appeler)

## Constructeurs

Méthode magique qui permet d'initialiser un objet. Il est appelé implicitement lorsque l'on essaie de créer un nouvel objet avec le mot clé new. 
On s'en sert souvent pour initialiser les attributs de nos objets. 

Un constructeur aura toujours une visibilité publique. Sinon on ne pourrait pas créer de nouveaux objets.


````php
    class MaClasse{
        private $attribut1;
        
        public function __construct($attribut1){
            $this->attribut1 = $attribut1;
        }
    }
````

Dans le cas ci-dessus, mon constructeur sera appelé quand je ferais : 
```php
    $objet = new MaClasse("Super attribut !");
```

Ici l'attribut attribut1 de mon objet de type MaClasse sera égal à "Super attribut !"

## Destructeurs

Le destructeur est une méthode magique appelée lors de la suppression de notre variable ou implicitement à la fin du script. 

````php
    class MaClasse{
        private $attribut1;
        
        public function __destruct(){
            echo('Mon objet est détruit')
        }
    }
````

## Héritage

L'héritage est une notion qui consiste à limiter la duplication de code source. 

Plutot que de répéter du code dans plusieurs classes on l'écrira une seule fois dans une classe parent toutes les classes qui hériteront de cette classe parent pourront bénéficier des attributs et méthodes qu'elle contient. 

```php
    class MaClassParent{
    
    }

    class MaClasseEnfant extends MaClasseParent{
    
    }
```

Ici, MaClasseEnfant héritera des attributs et des méthodes de MaClasseParent.

Si j'ai une méthode qui a le même nom dans une classe prent et dans une classe enfant, la méthode de la classe parent sera écrasée par celle de la classe enfant. 

On a souvent des méthodes qui ont le même noms par exemple le constructeur qui a une seule syntaxe possible. 

```php
    class MaClassParent{
        public function __construct(){
            echo('Je construit ma classe parent');
        }
    }

    class MaClasseEnfant extends MaClasseParent{
            public function __construct(){
            echo('Je construit ma classe enfant');
        }
    }

    $objet = new MaClasseEnfant();
```

Le code source ci-dessus affichera : "Je construit ma classe enfant sera appliquée".

Cela se produit car le constructeur de la classe enfant a écrasé celui de la classe parent. 

Néanmoins, je peux appeler celui de la classe parent en faisant : 

```php
    class MaClassParent{
        public function __construct(){
            echo('Je construit ma classe parent');
        }
    }

    class MaClasseEnfant extends MaClasseParent{
            public function __construct(){
            parent::__construct();
            echo('Je construit ma classe enfant');
        }
    }

    $objet = new MaClasseEnfant();
```

Dans ce cas mon programme affichera "Je construit ma classe parent" puis "Je construit ma classe enfant"

C'est ce quon appel une surcharge de méthode.


## Classe et méthodes abstraites

Une classe abstraite est une classe qui ne permettra pas de créer de nouveaux objets. 
En revanche, on pourra hériter de cette classe pour utiliser les attributs et méthodes qu'elle contient

````php
    abstract class MaClasseParent{
    
    }

    class MaClasseEnfant extends MaClasseParent{
    
    }

````

Dans le cas ci-dessus, il est impossible de créer un objet 
MaClasseParent par contre je peux créer des objets MaClasseEnfant qui hérite 
de MaClasseParent

```php
    // Ce code déclanchera une erreur 
    $object = new MaClasseParent();
    // Par contre celui là fonctionnera 
    $object2 = new MaClasseEnfant();
```

Une méthode abstraite est une méthode qui n'a pas de body 
on a simplement le nom de la méthode et les paramètres de la méthode si elle en a. 

Elle permet d'obliger l'utilisateur de la classe à développer une méthode

```php
    class MaClasse{
        abstract function sayHello();
    }
```

Si une classe hérite de MaClasse, elle devra impérativement implémenter une méthode sayHello()

````php
    class MaClasseEnfant extends MaClasse{
        public function sayHello(){
            echo('Hello');
        }
    }
````

Vu que la classe MaClasseEnfant étend de MaClasse, je suis obligé de développer la méthode sayHello()


## Attributs et méthodes statiques

Les attributs et les méthodes statique appartiennent à la classe et non à l'objet. Nous pouvons donc les utiliser sans créer de nouveaux objets. 

```php
    class MaClasse {
        static $staticAttr;
        
        static function methodeStatique(){
            echo('Appel d\'une méthode statique);
        }
    }

   
```

Pour appeler un attribut ou une méthode statique, j'utilise la syntaxe ci-dessous : 

```php
    // Affiche la valeur de mon attribut static staticAttr
    echo(MaClasse::$staticAttr);
    // Pour appeler une méthode statique
    MaClasse::methodeStatique();
```

Pour réccupérer un attribut statique au sein de mon objet, je peux utiliser le self

```php
    class MaClasse{
        static $attr = "toto";
        
        public function getStaticAttr(){
            return self::attr;
        }
    }
```

Le code source ci-dessous affichera "toto"

```php
    $objet = new MaClasse();
    echo($objet->getStaticAttr());
```