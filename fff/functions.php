<?php

function restrictAccess(){
    if(!array_key_exists("connecte", $_SESSION)){
        header("Location: login.php");
    }
}
function dbConnect(){
    // Nous nous connectons à notre base de données
    try {
        $host = 'database';
        $dbName = 'football';
        $user = 'root';
        $password = 'tiger';

        $pdo = new PDO(
            'mysql:host='.$host.';dbname='.$dbName.';charset=utf8',
            $user,
            $password);
        // Cette ligne demandera à pdo de renvoyer les erreurs SQL si il y en a
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Je retourne ma connexion
        return $pdo;
    }
    catch (PDOException $e) {
        throw new InvalidArgumentException('Erreur connexion à la base de données : '.$e->getMessage());
        exit;
    }
}

    function displayBsClassForm($errors, $input){
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            if(array_key_exists($input, $errors)){
                echo('is-invalid');
            } else {
                echo('is-valid');
            }
        }

    }

    function displayBsErrorForm($errors, $input){
        if(array_key_exists($input, $errors)){
            echo('<div  class="invalid-feedback">
        '.$errors[$input].'
      </div>');
        }
    }
    
    function keepFormValue($input, $playerEdit = null){
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            echo(htmlentities($_POST[$input]));
        } else {
            if(!is_null($playerEdit)){
                echo(htmlentities($playerEdit[$input]));
            }
        }
    }
?>