<html>
<head>

</head>
<body>
<h1>TodoList</h1>
<a href="index.php?difficulte=desc">Afficher du plus dure au plus simple</a> <br>
<a href="index.php?difficulte=asc">Afficher du plus simple au plus dure</a> <br>

<table>
    <thead>
    <th>Identifiant</th>
    <th>Libelle</th>
    <th>Difficulté</th>
    </thead>

    <tbody>
    <?php
    // Ici, je me connecte à ma base de données MySQL
    // N'oubliez pas d'adapter la valeur de $host, dbName et password
    // Ici ce sont les identifiants de MA base de données
    try {
        $host = 'database';
        $dbName = 'todolist';
        $user = 'root';
        $password = 'tiger';

        $pdo = new PDO(
            'mysql:host='.$host.';dbname='.$dbName.';charset=utf8',
            $user,
            $password);
        // Cette ligne demandera à pdo de renvoyer les erreurs SQL si il y en a
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $e) {
        throw new InvalidArgumentException('Erreur connexion à la base de données : '.$e->getMessage());
        exit;
    }

    // Je cré une variable resultats que j'initialise à null
    $resultats = null;

    // Si je n'ai pas de filtres dans mon url, je selectionne toutes les tâches
    if(!array_key_exists('difficulte', $_GET)){
        $query = $pdo->query("SELECT * FROM taches;");
        $resultats = $query->fetchAll();
    } else {
        // Sinon, si la difficulté est de la plus petite à la plus grande,
        // j'adapte ma requête MySQL en ajoutant ORDER BY difficulte ASC
        if($_GET["difficulte"] == 'asc'){
            $query = $pdo->query("SELECT * FROM taches ORDER BY difficulte ASC");
            $resultats = $query->fetchAll();
        }
        // Sinon, si j'ai cliqué sur le lien pour afficher du plus difficile au plus simple
        // j'adapte ma requête MySQL en ajoutant ORDER BY difficulte DESC
        if($_GET["difficulte"] == 'desc'){
            $query = $pdo->query("SELECT * FROM taches ORDER BY difficulte DESC");
            $resultats = $query->fetchAll();
        }
    }

    // J'affiche tous mes resultats
    foreach ($resultats as $resultat){
        echo(' <tr>
        <td>'.$resultat["id"].'</td>
        <td>'.$resultat["libelle"].'</td>
         <td>'.$resultat["difficulte"].'</td>
    </tr>');
    }


    ?>

    </tbody>
</table>

</body>
</html>