<?php
class Type {
    private $id;
    private $libelle;
    private $color;

    /**
     * @param $id
     * @param $libelle
     * @param $color
     */
    public function __construct($id, $libelle, $color)
    {
        $this->id = $id;
        $this->libelle = $libelle;
        $this->color = $color;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle): void
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color): void
    {
        $this->color = $color;
    }


}