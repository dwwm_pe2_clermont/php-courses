<html>
<head>

    <?php include 'View/parts/styles.php'; ?>
</head>
<body>

<div class="container">
    <h1>La voiture <?php echo($car->getModel());?></h1>
    <a class="btn btn-success" href="index.php?controller=car&action=list">Retour</a>


    <div class="picture">
        <?php
            if(!is_null($car->getPicture())){
        ?>
        <img class="img img-thumbnail" src="Public/uploads/<?php echo($car->getPicture()); ?> ">
        <?php
            } else {
                echo('<img class="img img-thumbnail" src="Public/images/no-picture.jpg">');
            }
        ?>
    </div>

    <ul>
        <li>Marque : <?php echo($car->getMarque()); ?></li>
        <li>Boite automatique : <?php echo(($car->getAutomatic())?'Oui':'Non'); ?></li>
        <li>Energy : <?php echo($car->getEnergy()); ?></li>
    </ul>
</div>
<?php include 'View/parts/scripts.php'; ?>
</body>
</html>