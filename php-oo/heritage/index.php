<?php
    require "CompteBancaire.php";
    require 'CompteProfessionnel.php';
    require 'CompteParticulier.php';

    $comptePro = new CompteProfessionnel(
        "0123443434",
        "0000000000000",
        "Human Booster");

    $comptePerso = new CompteParticulier(
        "34857385638457",
        "Delorme",
        "Aurélien",
        "3003934823947828");

    echo('Compte Professionnel :</br>');
    echo('Numero de compte : '.$comptePro->getNumeroCompte().'</br>');
    echo('Numéro de SIRET : '. $comptePro->getNumeroSiret()."</br>");
    echo('Raison sociale : '.$comptePro->getRaisonSociale()."</br>");

    echo('</br></br>');

    echo('Compte Personnel : </br>');
    echo('Numero de compte : '.$comptePerso->getNumeroCompte().'</br>');
    echo('Nom du titulaire : '.$comptePerso->getNomTitulaire().'</br>');
    echo('Prénom du titulaire : '.$comptePerso->getPrenomTitulaire().'</br>');
    echo('Numéro de sécu du titulaire : '.$comptePerso->getNumSecuTitulaire().'</br>');

?>