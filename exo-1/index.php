<?php
// Cette variable permet d'indiquer si un utilisateur est admin ou pas
$isAdmin = true;
// Contient le prénom de l'utilisateur connecté
$firstname = 'Aurélien';
$lastname = 'Delorme';

// Je cré une variable de type string
$variable1 = "1";
// Je cré une variable de type nombre
$variable2 = 1;

// Je regarde si elles sont égales
if($variable1 === $variable2){
    // J'affiche un var_dump
    var_dump("Je suis égal");
    // Je coupe mon script
    die();
}


$age = 19;

if($age<18){
    var_dump("Interdit tu es mineur");
   // die();
}

// Ternaire qui retourne 'majeur si mon age est supérieur ou égal à 18'
// Sinon il me retourne mineur
// Le retour est stocké dans la variable $isMajeur
$isMajeur = ($age>=18)?'majeur':'mineur';
var_dump($isMajeur);

$nombre = 14;
// Si le modulo de mon nombre par 2 est différent de 0
// (($nombre%2) != 0)
// Alors la variable isPair sera égale à "Le nombre est impair"
// Sinon elle sera égal à "Le nombre est pair"
$isPair = (($nombre%2)!=0)?
    "Le nombre est impair"
    :"Le nombre est pair";

var_dump($isPair);

$heure = 16;

if($heure < 11){
    var_dump("Je bois du café");
} else if($heure < 17){
    var_dump("Je bois de l'eau");
} else if ($heure< 23){
    var_dump("Je bois de la biere");
} else {
    var_dump("Je dors");
}


// Je cré un tableau vide
$myArray = [];
// J'ajoute un élément
$myArray[] = 'Pain au chocolat';
// J'ajoute un autre élément
$myArray[] = ['Croissants', 'Chouquettes'];
var_dump($myArray);
// La même chose sauf que
// j'indique directement les valeurs de mon tableau
$similarArray = ["Pain au chocolat", "Croissants"];
var_dump($similarArray);

$identity = [
        "nom"=> "Delorme",
        "prenom"=> "Aurélien",
        "email"=> "adelorme-ext@formateur-humanbooster.com"
];

var_dump($identity);

$panier = [
        [
                "prestation"=>"Blanchiement",
                "quantite"=>3,
                "prixUnitaireHt"=> 10.87
        ],
        [
            "prestation"=>"Suppression tache Ketchup",
            "quantite"=>2,
            "prixUnitaireHt"=> 11
        ]
];

var_dump($panier);

// Nous avons ici un tableau numéroté qui contient des tableaux clé => valeur
$utilisateurs = [
        [
                "nom"=> "Doe",
                "prenom"=> "John",
                "picture"=> "https://upload.wikimedia.org/wikipedia/commons/5/5a/John_Doe%2C_born_John_Nommensen_Duchac.jpg"
        ],
        [
                "nom"=> "Zidane",
                "prenom"=> "Zinedine",
                "picture"=> [
                        "photo1",
                        "photo2"
                ]
        ]
];



$utilisateurs[0]["ville"] = "Clermont-Ferrand";
unset($utilisateurs[0]["ville"]);
$utilisateurs[0]["prenom"] = $utilisateurs[0]["prenom"].' Prénom2';

var_dump($utilisateurs);
die();
?>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"></head>
<body>
<div class="container">

    <div class="w-100 text-center m-2">
    <svg xmlns="http://www.w3.org/2000/svg" width="251.156" height="28.113" viewBox="0 0 251.156 28.113">
        <g id="Group_42165" data-name="Group 42165" transform="translate(-49.578 -258.621)">
            <path id="Path_159646" data-name="Path 159646" d="M66.161,258.622a.994.994,0,0,1,.733.3,1.061,1.061,0,0,1,.3.753v26.013a1.064,1.064,0,0,1-.3.753,1,1,0,0,1-.733.3,1.016,1.016,0,0,1-1.05-1.05V273.719H51.658v11.966a1.066,1.066,0,0,1-.3.753,1.041,1.041,0,0,1-.733.3,1.016,1.016,0,0,1-1.05-1.05V259.672a1.017,1.017,0,0,1,1.05-1.05,1.038,1.038,0,0,1,.733.3,1.063,1.063,0,0,1,.3.753v11.966H65.111V259.672a1.017,1.017,0,0,1,1.05-1.05" transform="translate(0 -0.001)"></path>
            <path id="Path_159647" data-name="Path 159647" d="M129.321,300.962a1.016,1.016,0,0,1-1.05,1.05,1,1,0,0,1-.733-.3,1.065,1.065,0,0,1-.3-.753v-2.575a8.73,8.73,0,0,1-13.591.713,9.892,9.892,0,0,1-2.694-6.993V283.23a1.017,1.017,0,0,1,1.05-1.05,1.041,1.041,0,0,1,.733.3,1.063,1.063,0,0,1,.3.753v8.876a7.95,7.95,0,0,0,2.08,5.528,6.616,6.616,0,0,0,10.025,0,8.027,8.027,0,0,0,2.1-5.528V283.23a1.065,1.065,0,0,1,.3-.753,1,1,0,0,1,.733-.3,1.017,1.017,0,0,1,1.05,1.05Z" transform="translate(-39.802 -15.278)"></path>
            <path id="Path_159648" data-name="Path 159648" d="M195.46,282.18a7.592,7.592,0,0,1,5.983,2.912,10.467,10.467,0,0,1,2.477,7.013v8.856a1.03,1.03,0,0,1-.317.753,1.053,1.053,0,0,1-1.466,0,1.027,1.027,0,0,1-.317-.753v-8.856a8.532,8.532,0,0,0-1.863-5.547,5.53,5.53,0,0,0-8.975,0,8.471,8.471,0,0,0-1.882,5.547v8.856a1.064,1.064,0,0,1-.3.753,1.053,1.053,0,0,1-1.466,0,1.029,1.029,0,0,1-.317-.753v-8.856a8.531,8.531,0,0,0-1.862-5.547,5.549,5.549,0,0,0-8.995,0,8.5,8.5,0,0,0-1.862,5.528v8.875a1.063,1.063,0,0,1-.3.753,1.038,1.038,0,0,1-.733.3,1.016,1.016,0,0,1-1.05-1.05V283.23a1.017,1.017,0,0,1,1.05-1.05,1.039,1.039,0,0,1,.733.3,1.063,1.063,0,0,1,.3.753v2.358a7.651,7.651,0,0,1,6.36-3.408,7.351,7.351,0,0,1,4.358,1.427,9.456,9.456,0,0,1,3.051,3.724,9.288,9.288,0,0,1,3.031-3.724,7.41,7.41,0,0,1,4.358-1.427" transform="translate(-79.532 -15.278)"></path>
            <path id="Path_159649" data-name="Path 159649" d="M279.653,282.18a9.943,9.943,0,0,1,9.906,9.926v8.856a1.017,1.017,0,0,1-1.05,1.05,1.038,1.038,0,0,1-.733-.3,1.063,1.063,0,0,1-.3-.753v-8.856a7.846,7.846,0,1,0-2.338,5.567,1.04,1.04,0,0,1,.733-.3,1.017,1.017,0,0,1,1.05,1.05.965.965,0,0,1-.317.733,9.912,9.912,0,1,1-6.954-16.979" transform="translate(-142.763 -15.278)"></path>
            <path id="Path_159650" data-name="Path 159650" d="M344.336,282.18a8.538,8.538,0,0,1,6.5,2.912,9.966,9.966,0,0,1,2.694,7.013v8.856a1.016,1.016,0,0,1-1.05,1.05.994.994,0,0,1-.733-.3,1.063,1.063,0,0,1-.3-.753v-8.856a7.973,7.973,0,0,0-2.08-5.547,6.636,6.636,0,0,0-10.044,0,7.949,7.949,0,0,0-2.081,5.528v8.875a1.065,1.065,0,0,1-.3.753,1.04,1.04,0,0,1-.733.3,1.016,1.016,0,0,1-1.05-1.05V283.23a1.017,1.017,0,0,1,1.05-1.05,1.041,1.041,0,0,1,.733.3,1.065,1.065,0,0,1,.3.753v2.576a8.559,8.559,0,0,1,7.093-3.626" transform="translate(-185.198 -15.278)"></path>
            <path id="Path_159651" data-name="Path 159651" d="M406.638,266.336a10.191,10.191,0,1,1-10.209,10.189V260.56a1.965,1.965,0,0,1,1.938-1.938,1.885,1.885,0,0,1,1.381.575,1.853,1.853,0,0,1,.556,1.362v15.966a6.31,6.31,0,1,0,6.333-6.313,6.346,6.346,0,0,0-2.571.537,1.938,1.938,0,1,1-1.555-3.55,10.108,10.108,0,0,1,4.126-.863" transform="translate(-224.928 -0.001)"></path>
            <path id="Path_159652" data-name="Path 159652" d="M590.433,290.855a6.771,6.771,0,0,1,.019,8.806,6.11,6.11,0,0,1-4.817,2.07h-5.683a1.927,1.927,0,0,1-1.9-1.9,1.848,1.848,0,0,1,.565-1.355,1.787,1.787,0,0,1,1.336-.565h5.683a2.524,2.524,0,0,0,1.976-.771,2.734,2.734,0,0,0,.64-1.863,2.952,2.952,0,0,0-.358-1.43q-.64-1.223-4.422-1.223a5.137,5.137,0,0,1-4.027-1.731,5.663,5.663,0,0,1-.038-7.4,5.157,5.157,0,0,1,4.065-1.75H589a1.9,1.9,0,0,1,1.9,1.9,1.787,1.787,0,0,1-.565,1.336,1.828,1.828,0,0,1-1.336.565h-5.532a1.52,1.52,0,0,0-1.2.47,1.708,1.708,0,0,0-.4,1.167,1.431,1.431,0,0,0,1.6,1.637q5.155,0,6.963,2.032" transform="translate(-342.707 -14.997)"></path>
            <path id="Path_159653" data-name="Path 159653" d="M631.369,270.532v6.191a6.2,6.2,0,0,0,6.21,6.191,1.928,1.928,0,0,1,1.9,1.9,1.848,1.848,0,0,1-.564,1.355,1.827,1.827,0,0,1-1.336.565,10.023,10.023,0,0,1-10.011-10.011v-16.2a1.826,1.826,0,0,1,.564-1.336,1.786,1.786,0,0,1,1.336-.565,1.895,1.895,0,0,1,1.9,1.9v6.21h6.21a1.895,1.895,0,0,1,1.9,1.9,1.788,1.788,0,0,1-.564,1.336,1.827,1.827,0,0,1-1.336.565Z" transform="translate(-374.818)"></path>
            <path id="Path_159654" data-name="Path 159654" d="M684.009,288.111v.019a1.9,1.9,0,0,1-1.054,2.465L672.3,294.866a1.9,1.9,0,0,1-.7.132,1.75,1.75,0,0,1-1.769-1.2,1.9,1.9,0,0,1,1.054-2.465l8.637-3.462a6.188,6.188,0,1,0,.019,7.621,1.848,1.848,0,0,1,1.279-.715,1.815,1.815,0,0,1,1.393.4,1.877,1.877,0,0,1,.339,2.672,9.528,9.528,0,0,1-7.885,3.857,10,10,0,1,1,0-20,10.029,10.029,0,0,1,9.333,6.4Z" transform="translate(-398.875 -14.962)"></path>
            <path id="Path_159655" data-name="Path 159655" d="M739.462,281.844a1.8,1.8,0,0,1,1.223.791,1.82,1.82,0,0,1,.3,1.43,1.887,1.887,0,0,1-.791,1.223,1.82,1.82,0,0,1-1.43.3,4.427,4.427,0,0,0-.9-.094,5.028,5.028,0,0,0-3.895,1.825,6.385,6.385,0,0,0-1.637,4.366v8.11a1.817,1.817,0,0,1-.545,1.336,1.849,1.849,0,0,1-1.355.565,1.928,1.928,0,0,1-1.9-1.9v-16.2a1.928,1.928,0,0,1,1.9-1.9,1.848,1.848,0,0,1,1.355.565,1.817,1.817,0,0,1,.545,1.336v.038a8.724,8.724,0,0,1,5.532-1.938,8.548,8.548,0,0,1,1.6.15" transform="translate(-440.29 -14.962)"></path>
            <path id="Path_159656" data-name="Path 159656" d="M489.651,281.694a10.189,10.189,0,0,0-7.419,3.346,18.648,18.648,0,0,0-3.837,6.08,15.019,15.019,0,0,1-3.232,5.011,6.817,6.817,0,0,1-4.813,2.154,6.59,6.59,0,1,1,0-13.18c3.521,0,5.864,3.032,7.073,5.151.6-1.487,1.184-2.759,1.647-3.705-1.815-2.4-4.66-4.857-8.719-4.857a10,10,0,1,0,7.194,16.88,18.523,18.523,0,0,0,4.061-6.3,15.374,15.374,0,0,1,3.063-4.845,6.909,6.909,0,0,1,4.983-2.323,6.59,6.59,0,1,1,0,13.18c-3.547,0-5.9-3.079-7.1-5.2-.6,1.494-1.193,2.769-1.654,3.708,1.813,2.419,4.668,4.9,8.754,4.9a10,10,0,1,0,0-20" transform="translate(-266.378 -14.962)"></path>
        </g>
    </svg>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Mon super pressing !</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <?php
                // Permet d'afficher un lien vers l'admin si l'utilisateur est connecté
                 if($isAdmin){
                     echo('<li class="nav-item">
                    <a class="nav-link" href="admin-space.php">Administration !</a>
                </li>');
                 }
                ?>


            </ul>
            <div class="my-2 my-lg-0">
                <?php
                if($isAdmin){
                    echo('<i class="fa fa-lock" style="font-size:48px;"></i>');
                } else {

                    echo('<i class="fa fa-user"></i>');
                }

                echo("Bonjour ". $firstname." ".$lastname);
                ?>
            </div>
        </div>
    </nav>

<h1>Mon super pressing !</h1>




</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>
