<?php
class PrixNegatifException extends \Exception{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct("Ce produit a un prix négatif", $code, $previous);
    }
}