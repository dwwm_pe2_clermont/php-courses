<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">FFF</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.php">Voir le site internet</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin-index.php">Administration</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin-logout.php">Me déconnecter</a>
                </li>
            </ul>
        </div>
    </div>
</nav>