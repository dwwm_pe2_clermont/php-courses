<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <?php
        include 'View/parts/nav.php'
    ?>
    <h1 class="text-center">
        <?php
            echo($techno->getNom())
        ?>
    </h1>

    <a class="btn btn-success" href="index.php?controller=techno&action=list">Revenir en arrière</a>

    <div class="col-md-12 text-center">
        <h2><?php echo($techno->getCategory())?></h2>
        <img style="max-width: 100%" src="<?php echo($techno->getImage())?>">
    </div>

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>