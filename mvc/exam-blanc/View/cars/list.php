<html>
<head>

    <?php include 'View/parts/styles.php'; ?>
</head>
<body>

<div class="container">
    <h1>Les voitures !</h1>

    <a class="btn btn-success" href="index.php?controller=car&action=add">Ajouter un véhicule</a>

    <?php
    if(count($cars) == 0){
        echo('<h1 class="text-center text-danger">
                Il n\'y a pas de véhicules en base
                </h1>');
    } else {
    ?>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Model</th>
                <th scope="col">Marque</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($cars as $car){
                echo(' <tr>
                <th scope="row">'.$car->getId().'</th>
                <td>'.$car->getModel().'</td>
                <td>'.$car->getMarque().'</td>
                <td>
                <a href="index.php?controller=car&action=detail&id='.$car->getId().'">Détail</a>
                <a href="index.php?controller=car&action=delete&id='.$car->getId().'">Supprimer</a>
                </td>
            </tr>');
            }
            ?>


            </tbody>
        </table>
    <?php
    }
    ?>

</div>
<?php include 'View/parts/scripts.php'; ?>
</body>
</html>