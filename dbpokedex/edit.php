<?php
require 'functions.php';
checkConnection();

$pdo = dbConnect();

$id = $_GET["id"];

// On selectionne le pokemon a mettre a jour
$query = $pdo->prepare("SELECT * FROM pokemon WHERE id = :id");
$query->execute(["id"=> $id]);
$result = $query->fetch();

$errors = [];

// Si notre formulaire est soumis on passe dans cette condition
if($_SERVER["REQUEST_METHOD"] == "POST"){

    // On valide les erreurs de notre form
    $errors = validateForm();

    // On vérifie qu'il y a bien eu un upload
    if($_FILES["image"]["error"] != 4){
        // On regarde si notre est valide
        // En utilisant une fonction pour réutiliser le code de l'ajout
        $errorUpload = testUpload();
        // Si il y a des erreurs, on les ajoute dans notre tableau d'erreur
        if(!is_null($errorUpload)){
            $errors["image"] = $errorUpload;
        }
    }

    if(count($errors) == 0){
        // Si il n'y a pas d'erreurs et qu'il y a un upload

        if($_FILES["image"]["error"] != 4){
            // On remplace l'ancien fichier par le nouveau
            // en l'écrasant
            move_uploaded_file($_FILES["image"]["tmp_name"],
                "uploads/images_pokemon/". $result["image"]);
        }

        $request = $pdo->prepare("UPDATE pokemon SET 
                   nom = :nom, 
                   pv = :pv, 
                   attaque = :attaque, 
                   defense = :defense, 
                   vitesse = :vitesse, 
                   special = :special, 
                   type_id = :type
                   WHERE id = :id");

        $request->execute([
                "nom"=> $_POST["nom"],
                "pv"=> $_POST["pv"],
                "attaque"=> $_POST["attaque"],
                "defense"=> $_POST["defense"],
                "vitesse"=> $_POST["vitesse"],
                "special"=> $_POST["special"],
                "type"=> $_POST["type"],
                "id"=> $_GET["id"]
        ]);

        header("Location: admin-index.php");
    }
}
?>


<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous"/>
</head>
<body>
    <div class="container">
        <?php
        include 'parts/navbar.php';
        ?>

        <form method="post" enctype="multipart/form-data">
            <div class="form-group m-2">
                <label>Nom</label>
                <input class="form-control <?php displayValidationBootstrapClass($errors, 'nom') ?>"
                       type="text" name="nom"
                       value="<?php displayFormValue('nom', $result);;?>"
                       placeholder="Nom du pokemon">

                <?php displayFormError($errors, 'nom')?>
            </div>

            <div class="form-group m-2">
                <label>Image</label>
                <div class="mb-3">
                    <label>Pour modifier cette image, uploadez une nouvelle</label>
                    <img style="max-width: 100px;max-height: 100px;" class="img-thumbnail" src="uploads/images_pokemon/<?php echo($result["image"])?>?id=<?php echo(uniqid());?>">
                </div>
                <input
                       class="form-control <?php displayValidationBootstrapClass($errors, 'image') ?>" type="file" name="image" placeholder="Lien vers l'image">
                <?php displayFormError($errors, 'image')?>
            </div>

            <div class="form-group m-2">
                <label>Points de vie</label>
                <input value="<?php displayFormValue('pv', $result);;?>"
                       class="form-control <?php displayValidationBootstrapClass($errors, 'pv') ?>"
                       type="number" name="pv" placeholder="Points de vie">

                <?php displayFormError($errors, 'pv')?>
            </div>

            <div class="form-group m-2">
                <label>Attaque</label>
                <input value="<?php displayFormValue('attaque', $result);?>"
                       class="form-control <?php displayValidationBootstrapClass($errors, 'attaque') ?>"
                       type="number" name="attaque" placeholder="Attaque">
                <?php displayFormError($errors, 'attaque')?>
            </div>

            <div class="form-group m-2">
                <label>Défense</label>
                <input value="<?php displayFormValue('defense', $result);?>"
                       class="form-control <?php displayValidationBootstrapClass($errors, 'defense') ?>"
                       type="number" name="defense" placeholder="Défense">
                <?php displayFormError($errors, 'defense')?>
            </div>

            <div class="form-group m-2">
                <label>Vitesse</label>
                <input value="<?php displayFormValue('vitesse', $result);?>"
                       class="form-control <?php displayValidationBootstrapClass($errors, 'vitesse') ?>"
                       type="number" name="vitesse" placeholder="Vitesse">
                <?php displayFormError($errors, 'vitesse')?>
            </div>

            <div class="form-group m-2">
                <label>Spécial</label>
                <input value="<?php displayFormValue('special', $result);?>"
                       class="form-control <?php displayValidationBootstrapClass($errors, 'special') ?>"
                       type="number" name="special" placeholder="Special">
                <?php displayFormError($errors, 'special')?>
            </div>

            <div class="form-group m-2">
                <label>Type de pokemon</label>
                <select name="type"
                        class="form-select <?php displayValidationBootstrapClass($errors, 'type') ?>">

                    <?php
                    $req = $pdo->query("SELECT * FROM type");
                    $results = $req->fetchAll();
                    foreach ($results as $item){
                        $actif = '';
                        if(($_SERVER["REQUEST_METHOD"] == 'POST'
                            && $_POST["type"] == $item["id"]) ||
                            ($_SERVER["REQUEST_METHOD"] != 'POST'
                                && $result["type_id"] == $item["id"])){
                            $actif = 'selected';
                        }
                        echo('<option '.$actif.' value="'.$item["id"].'">'.$item["libelle"].'</option>');
                    }
                    ?>
                </select>
                <?php displayFormError($errors, 'type')?>
            </div>

            <input type="submit" class="btn btn-success m-2">
        </form>
    </div>
</body>
