<?php
abstract class DbManager{
    protected $bdd;

    private $host = 'database';
    private $username = 'root';
    private $password = 'tiger';
    private $dbName = 'techno_crud';

    public function __construct(){
        $this->bdd = new PDO("mysql:host=".$this->host.";dbname=".$this->dbName,
            $this->username,
            $this->password);
    }
}