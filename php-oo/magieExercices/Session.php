<?php
class Session{
    private $nom;
    private $prenom;
    private $dateReveil;

    public function __construct($nom, $prenom){
        $this->nom = $nom;
        $this->prenom = $prenom;
    }
    public function __get($name){
        echo($name.' est inaccessible');
    }

    public function __set($key, $value){
        echo('Impossible d\'affecter la valeur '
            .$value.' à l\'attribut '.$key.
            ' qui est inaccessible');
    }

    public function __isset($key){
        echo('Impossible d\'appeler la fonction 
        php isset sur l\'attribut '.$key.
            ' qui est inaccessible');
    }

    public function __unset($key){
        echo('Impossible d\'appeler la fonction php unset
        sur l\'attribut '.$key.' qui est inaccessible');
    }

    public function __sleep(){
        return [
            'nom', 'prenom'
        ];
    }

    public function __wakeup(){
        $this->dateReveil = new DateTime();
    }

    public function __toString(){
        return 'Je suis un objet avec 
        un attribut prénom qui vaut '
            .$this->prenom.' un attribut nom qui vaut '
            .$this->nom.' je me suis réveillé à '
            .$this->dateReveil->format('d/m/Y H:i:s');
    }
}