<?php
class Chien implements AnimalInterface, AnimalTerrestreInterface {

    public function makeSound()
    {
        echo('Ouaf !');
    }

    public function courir()
    {
        echo('Je cours !');
    }
}