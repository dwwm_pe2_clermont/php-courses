<?php
    abstract class Animal {

        public static $typeAnimals = ["Chien", "Chat"];

        public static function getTypeAnimals(){
            var_dump(self::$typeAnimals);
        }

        public abstract function sayHello();

        public function manger(){
            echo('Je mange');
        }

        public function boire(){
            echo("Je bois");
        }

        public function dormir(){
            echo('Je dors');
        }
    }
?>