<?php
require 'functions.php';
$pdo = dbConnect();
$query = $pdo->query("SELECT * FROM player WHERE poste = 'gardien'");
$gardiens = $query->fetchAll();

$query = $pdo->query("SELECT * FROM player WHERE poste = 'defenseur'");
$defenseurs = $query->fetchAll();

$query = $pdo->query("SELECT * FROM player WHERE poste = 'milieu'");
$milieus = $query->fetchAll();


$query = $pdo->query("SELECT * FROM player WHERE poste = 'attaquant'");
$attaquants = $query->fetchAll();
?>
<html>
<head>
    <?php
    include 'parts/stylesheets.php';
    ?>
</head>
<body>

<div class="container">
   <h1>Les joueurs selectionnés</h1>

    <h2>Les gardiens</h2>

    <div class="row">
    <?php
        if(count($gardiens) == 0){
            echo('<div class="text-danger">Il n\'y a pas de gardiens dans l\'équipe</div>');
        } else {
            foreach ($gardiens as $gardien){
                $date = new \DateTime($gardien["date_naissance"]);
                echo('<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">'.strtoupper(htmlentities($gardien["nom"])).' '.htmlentities($gardien["prenom"]).'</h5>
    <h6 class="card-subtitle mb-2 text-muted">'.$date->format('d/m/Y').'</h6>
  </div>
</div>');
            }
        }
    ?>
    </div>


    <h2>Les défenseurs</h2>

    <div class="row">
        <?php
        if(count($defenseurs) == 0){
            echo('<div class="text-danger">Il n\'y a pas de défenseurs dans l\'équipe</div>');
        } else {
            foreach ($defenseurs as $defenseur){
                $date = new \DateTime($defenseur["date_naissance"]);
                echo('<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">'.strtoupper(htmlentities($defenseur["nom"])).' '.htmlentities($defenseur["prenom"]).'</h5>
    <h6 class="card-subtitle mb-2 text-muted">'.$date->format('d/m/Y').'</h6>
  </div>
</div>');
            }
        }
        ?>
    </div>


    <h2>Les millieux</h2>

    <div class="row">
        <?php
        if(count($milieus) == 0){
            echo('<div class="text-danger">Il n\'y a pas de défenseurs dans l\'équipe</div>');
        } else {
            foreach ($milieus as $milieu){
                $date = new \DateTime($milieu["date_naissance"]);
                echo('<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">'.strtoupper(htmlentities($milieu["nom"])).' '.htmlentities($milieu["prenom"]).'</h5>
    <h6 class="card-subtitle mb-2 text-muted">'.$date->format('d/m/Y').'</h6>
  </div>
</div>');
            }
        }
        ?>
    </div>

    <h2>Les attaquants</h2>

    <div class="row">
        <?php
        if(count($attaquants) == 0){
            echo('<div class="text-danger">Il n\'y a pas de défenseurs dans l\'équipe</div>');
        } else {
            foreach ($attaquants as $attaquant){
                $date = new \DateTime($attaquant["date_naissance"]);
                echo('<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">'.strtoupper(htmlentities($attaquant["nom"])).' '.htmlentities($attaquant["prenom"]).'</h5>
    <h6 class="card-subtitle mb-2 text-muted">'.$date->format('d/m/Y').'</h6>
  </div>
</div>');
            }
        }
        ?>
    </div>
</div>

<?php
include 'parts/javascripts.php';
?>
</body>
</html>