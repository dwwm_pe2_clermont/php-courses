<?php
    require "View/parts/function.php";
?>
<html>
<head>

    <?php include 'View/parts/styles.php'; ?>
</head>
<body>

<div class="container">
    <h1>Ajouter une voiture</h1>
    <a class="btn btn-success" href="index.php?controller=car&action=list">Retour</a>

    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Marque du véhicule</label>
            <input class="form-control <?php echo(displayBsClass('marque', $errors));?>"
                   name="marque" value="<?php echo(keepValue("marque"));?>" placeholder="Marque">

            <?php
                displayErrorMessage("marque", $errors);
            ?>

        </div>

        <div class="form-group">
            <label>Modèle du véhicule</label>
            <input class="form-control <?php echo(displayBsClass('model', $errors));?>"
                   value="<?php echo(keepValue("model"));?>" name="model" placeholder="Modèle">


            <?php
            displayErrorMessage("model", $errors);
            ?>
        </div>

        <div class="mt-2 form-group <?php echo(displayBsClass("energy", $errors)); ?>">
            <label>Energie du véhicule</label>
            <br>
            <?php
                foreach (Car::$allowedEnergy as $energy){
                    $class = displayBsClass("energy", $errors);
                    $checked = '';
                    if(array_key_exists("energy", $_POST) && $energy == $_POST["energy"]){
                        $checked = "checked";
                    }
                    echo('  <div class="form-check form-check-inline">
                <input '.$checked.' class="form-check-input '.$class.'" type="radio" name="energy" id="inlineRadio1" value="'.$energy.'">
                <label class="form-check-label" for="inlineRadio1">'.$energy.'</label>
            </div>');
                }
            ?>

        </div>

        <div class="form-group">
            <br>
            <div class="form-check form-switch">
                <?php
                $checked = '';
                if(array_key_exists('automatic', $_POST) && $_POST["automatic"] == 'on'){
                    $checked = 'checked';
                }
                echo(' <input '.$checked.' class="form-check-input" name="automatic" type="checkbox" id="flexSwitchCheckChecked">')
                ?>
                <label class="form-check-label" for="flexSwitchCheckChecked">Le véhicule dispose d'une boite auto</label>

            </div>
        </div>

        <div class="form-group mt-2">
            <input type="file" name="picture" class="form-control <?php echo(displayBsClass("picture", $errors)); ?>">
            <?php
            displayErrorMessage("picture", $errors);
            ?>
        </div>

        <input type="submit" class="btn btn-success mt-3">
    </form>

</div>
<?php include 'View/parts/scripts.php'; ?>
</body>
</html>