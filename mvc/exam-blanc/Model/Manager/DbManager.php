<?php
abstract class DbManager{
    protected $bdd;

    private $host = 'database';
    private $username = 'root';
    private $password = 'tiger';
    private $dbName = 'exam_blanc';

    public function __construct(){
        $this->bdd = new PDO("mysql:host=".$this->host.";dbname=".$this->dbName,
            $this->username,
            $this->password);
    }
}