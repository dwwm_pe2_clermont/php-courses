<?php
    // session_start
    require "functions.php";
    $pdo = dbConnect();
$errors = null;
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $query = $pdo->prepare("SELECT * FROM users WHERE email = :email");
        $query->execute([
            "email"=> $_POST["email"]
        ]);
        $results = $query->fetch();



        if(!$results){
            $errors = "Les identifiants sont incorrects";
        } elseif(!password_verify($_POST["password"], $results["password"])){
            $errors = "Les identifiants sont incorrects";
        } else {
            $_SESSION["user"] = $results;
            header("Location: admin-index.php");
        }
    }

?>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>Me connecter</h1>
    <form method="post">

        <div class="form-control">
            <label>Email de l'utilisateur</label>
            <input class="form-control" type="text" name="email" placeholder="Email">
        </div>

        <div class="form-control">
            <label>Mot de passe de l'utilisateur</label>
            <input class="form-control" type="password" name="password" placeholder="Mot de passe">
        </div>

        <?php
        if(!is_null($errors)){
            echo('<div class="text-danger">'.$errors.'</div>');
        }
        ?>
        <input type="submit" class="btn btn-success mt-3">
    </form>

    <a href="register.php">Créer un compte !</a>
</div>
</body>
