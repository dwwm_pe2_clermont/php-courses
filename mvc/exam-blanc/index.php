<?php
require "loader.php";

if(empty($_GET)){
    header("Location: index.php?controller=car&action=list");
}

$exceptionController = new ExceptionController();
if($_GET["controller"] == "car"){
    $controller = new CarController();

    if($_GET["action"] == "list"){
        $controller->list();
    } else if($_GET["action"] == "add"){
        $controller->add();
    }else if($_GET["action"] == 'detail' && array_key_exists("id", $_GET)){
        $controller->detail($_GET["id"]);
    } else if($_GET["action"] == 'delete' && array_key_exists("id", $_GET)){
        $controller->delete($_GET["id"]);
    } else {
        $exceptionController->notFound();
    }
} else {
    $exceptionController->notFound();
}