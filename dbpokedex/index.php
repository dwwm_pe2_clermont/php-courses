<?php
include 'functions.php';
checkConnection();
$pdo = dbConnect();

// On selectionne tous nos pokemons
$query = $pdo->query("SELECT * FROM pokemon");
// On les récupère sous forme de tableau

$resultats = $query->fetchAll();

?>

<html>
<head>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous"/>
</head>
<body>

<div class="container">
    <?php
    include 'parts/navbar.php'
    ?>
    <h1>Mes pokemons !</h1>
    <div class="row">
        <?php
            // On parcours notre tableau de pokemon pour les afficher !
            foreach ($resultats as $resultat){
                echo('<div class="card" style="width: 18rem;">
  <img src="uploads/images_pokemon/'.$resultat["image"].'?'.uniqid().'" class="card-img-top" alt="Pokemon '.$resultat["nom"].'">
  <div class="card-body">
    <h5 class="card-title">'.$resultat["nom"].'</h5>
    <a href="detail.php?id='.$resultat["id"].'" class="btn btn-primary">Voir la ficher détaillée</a>
    <a href="edit.php?id='.$resultat["id"].'" class="btn btn-warning">Modifier le pokemon</a>
    <!-- On fait un lien vers un fichier de suppression qui prend en paramètre l\'id à supprimer -->
     <a href="admin-delete.php?id='.$resultat["id"].'" class="btn btn-danger">Supprimer le pokemon</a>
  </div>
</div>');
            }
        ?>
    </div>

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

</body>
</html>

