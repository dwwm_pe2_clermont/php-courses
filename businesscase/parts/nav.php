<nav class="navbar navbar-expand-lg
<?php if(array_key_exists("theme",$_COOKIE)
&& $_COOKIE["theme"] == "dark")
{echo ('navbar.php-dark bg-dark');} else {
    echo('navbar.php-light bg-light');
}?>">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="home.php">Accueil</a>
                </li>
                <?php
                    if(!array_key_exists("username", $_SESSION)){
                        echo('<li class="nav-item">
                            <a class="nav-link" href="register.php">Créer un compte</a>
                        </li>
                          <li class="nav-item">
                    <a class="nav-link" href="login.php">Me connecter</a>
                </li>');
                    }
                ?>

                <?php
                    if(array_key_exists("username", $_SESSION)){
                        echo(' <li class="nav-item">
                    <a class="nav-link" href="logout.php">Me déconnecter</a>
                </li>');

                        echo(' <li class="nav-item">
                    <a class="nav-link" href="my-account.php">Mon compte</a>
                </li>');
                    }
                ?>

                <li class="nav-item"><a class="nav-link" href="add_picture.php">Télécharger une nouvelle image</li>
            </ul>

        </div>

        <ul class="navbar-nav mb-2 mb-lg-0">
            <?php
            if(array_key_exists("username", $_SESSION)){
                echo('Bonjour '. $_SESSION["username"]);
            }
            ?>

            <?php
                if(array_key_exists("theme", $_GET)){
                    setcookie("theme", $_GET["theme"], time()+60*60*24);
                    $getParams = '';

                    if(array_key_exists("id", $_GET)){
                        $getParams = '?id='.$_GET["id"];
                    }

                    $urlRedirection = $_SERVER['REQUEST_URI'];
                    $urlRedirection = explode("?",$urlRedirection)[0].$getParams;
                    header("Location: ".$urlRedirection);
                    exit();
                }
            ?>
            <?php
                if(array_key_exists("consent", $_COOKIE) && $_COOKIE["consent"] == "yes"){
                    if(array_key_exists("theme", $_COOKIE) && $_COOKIE["theme"] == 'dark'){
                        echo('<a href="?theme=light"><i class="fa fa-solid fa-sun"></i></a>');
                    } else if (array_key_exists("theme", $_COOKIE) && $_COOKIE["theme"] == 'light'){
                        echo('<a href="?theme=dark"><i class="fa fa-solid fa-moon"></i></a>');
                    } else {
                        echo('<a href="?theme=light"><i class="fa fa-solid fa-sun"></i></a>');
                        echo('<a href="?theme=dark"><i class="fa fa-solid fa-moon"></i></a>');
                    }


                }
            ?>
        </ul>
    </div>
</nav>