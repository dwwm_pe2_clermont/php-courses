<?php
    $query = $pdo->query("SELECT DISTINCT(t.libelle), t.id
        FROM type t 
            INNER JOIN pokemon p 
                ON t.id = p.type_id");

    $resultatType = $query->fetchAll();
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="index.php">Pokedex</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <div class="container-fluid">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="index.php">Home</a>
                </li>

                <?php
                    foreach ($resultatType as $res){
                        echo(' <li class="nav-item">
                    <a class="nav-link" href="type.php?type='.$res["id"].'">'.$res["libelle"].'</a>
                </li>');
                    }
                ?>

                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="pokemon-add.php">Ajouter un pokemon</a>
                </li>
            </div>
            <div class="d-flex col-md-3">
                Bonjour <?php echo($_SESSION["user"]["firstname"]. ' ' . strtoupper($_SESSION["user"]["lastname"]));?>
                &nbsp;
                <a href="logout.php">
                    <i class="fa fa-lock"></i>
                </a>

            </div>
        </div>
    </div>
</nav>