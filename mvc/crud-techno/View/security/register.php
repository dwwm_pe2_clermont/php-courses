<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <a class="btn btn-success mb-3" href="index.php?controller=security&action=login">Retourner sur le login</a>
    <h1>Créer un compte !</h1>
    <form method="post">
        <div class="form-group">
            <label>Email</label>
            <input type="text" value="<?php if(array_key_exists("email", $_POST)){echo($_POST["email"]);}?>" name="email"
                   class="form-control  <?php if(array_key_exists("email", $errors)){echo('is-invalid');}?>">
            <?php
            if(array_key_exists("email", $errors)){
                echo('<div class="invalid-feedback">'.$errors["email"].'</div>');
            }
            ?>
        </div>

        <div class="form-group">
            <label>Mot de passe</label>
            <input type="password" name="password" class="form-control <?php if(array_key_exists("password", $errors)){echo('is-invalid');}?>">
            <?php
            if(array_key_exists("password", $errors)){
                echo('<div class="invalid-feedback">'.$errors["password"].'</div>');
            }
            ?>
        </div>


        <div class="form-group">
            <label>Confirmation du mot de passe</label>
            <input type="password" name="confirm_password" class="form-control <?php if(array_key_exists("confirm_password", $errors)){echo('is-invalid');}?>">
            <?php
            if(array_key_exists("confirm_password", $errors)){
                echo('<div class="invalid-feedback">'.$errors["confirm_password"].'</div>');
            }
            ?>
        </div>

        <input type="submit" class="btn btn-success mt-3">
    </form>
</div>
</body>
</html>
