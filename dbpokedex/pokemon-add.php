<?php
    include 'functions.php';

    checkConnection();
    $pdo = dbConnect();

    $errors = [];

    if($_SERVER["REQUEST_METHOD"] == 'POST'){

        $errors = validateForm();


        $errorUpload = testUpload();
        if(!is_null($errorUpload)){
            $errors["image"] = $errorUpload;
        }



        if(count($errors) == 0){

            $filePath = uniqid().$_FILES["image"]["name"];
            move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/images_pokemon/".$filePath);

            $req = $pdo->prepare("INSERT INTO Pokemon (image, nom, pv, attaque, defense, vitesse, special, type_id) 
                                        VALUES 
                            (:image, :nom, :pv, :attaque, :defense, :vitesse, :special, :type_id)");

            $req->execute(
                    [
                        "image"=> $filePath,
                        "nom"=> $_POST["nom"],
                        "pv"=> $_POST["pv"],
                        "attaque"=> $_POST["attaque"],
                        "defense"=> $_POST["defense"],
                        "vitesse"=> $_POST["vitesse"],
                        "special"=> $_POST["special"],
                        "type_id"=> $_POST["type"]
                    ]
            );

            header("Location: admin-index.php");
        }
    }
?>

<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous"/>
</head>
<body>

<div class="container">

    <?php
        include 'parts/navbar.php';
    ?>

    <h1>Ajouter un pokemon !</h1>

    <form method="post" enctype="multipart/form-data">
        <div class="form-group m-2">
            <label>Nom</label>
            <input class="form-control <?php displayValidationBootstrapClass($errors, 'nom') ?>"
                   type="text" name="nom"
                   value="<?php displayFormValue('nom');?>"
                   placeholder="Nom du pokemon">

            <?php displayFormError($errors, 'nom')?>
        </div>

        <div class="form-group m-2">
            <label>Image</label>
            <input class="form-control <?php displayValidationBootstrapClass($errors, 'image') ?>" type="file" name="image" placeholder="Lien vers l'image">
            <?php displayFormError($errors, 'image')?>
        </div>

        <div class="form-group m-2">
            <label>Points de vie</label>
            <input value="<?php displayFormValue('pv');?>"
                    class="form-control <?php displayValidationBootstrapClass($errors, 'pv') ?>"
                   type="number" name="pv" placeholder="Points de vie">

            <?php displayFormError($errors, 'pv')?>
        </div>

        <div class="form-group m-2">
            <label>Attaque</label>
            <input value="<?php displayFormValue('attaque');?>"
                    class="form-control <?php displayValidationBootstrapClass($errors, 'attaque') ?>"
                   type="number" name="attaque" placeholder="Attaque">
            <?php displayFormError($errors, 'attaque')?>
        </div>

        <div class="form-group m-2">
            <label>Défense</label>
            <input value="<?php displayFormValue('defense');?>"
                    class="form-control <?php displayValidationBootstrapClass($errors, 'defense') ?>"
                   type="number" name="defense" placeholder="Défense">
            <?php displayFormError($errors, 'defense')?>
        </div>

        <div class="form-group m-2">
            <label>Vitesse</label>
            <input value="<?php displayFormValue('vitesse');?>"
                    class="form-control <?php displayValidationBootstrapClass($errors, 'vitesse') ?>"
                   type="number" name="vitesse" placeholder="Vitesse">
            <?php displayFormError($errors, 'vitesse')?>
        </div>

        <div class="form-group m-2">
            <label>Spécial</label>
            <input value="<?php displayFormValue('special');?>"
                    class="form-control <?php displayValidationBootstrapClass($errors, 'special') ?>"
                   type="number" name="special" placeholder="Special">
            <?php displayFormError($errors, 'special')?>
        </div>

        <div class="form-group m-2">
            <label>Type de pokemon</label>
        <select name="type"
                class="form-select <?php displayValidationBootstrapClass($errors, 'type') ?>">

        <?php
            $req = $pdo->query("SELECT * FROM type");
            $results = $req->fetchAll();
            foreach ($results as $result){
                $actif = '';
                if($_SERVER["REQUEST_METHOD"] == 'POST'
                    && $_POST["type"] == $result["id"]){
                $actif = 'selected';
                }
                echo('<option '.$actif.' value="'.$result["id"].'">'.$result["libelle"].'</option>');
            }
        ?>
        </select>
            <?php displayFormError($errors, 'type')?>
        </div>

        <input type="submit" class="btn btn-success m-2">
    </form>


</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>

