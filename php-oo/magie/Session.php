<?php
    class Session{

        public $nom;
        public $prenom;
        public $heureReveil;

        public function __construct($nom, $prenom){
            $this->nom = $nom;
            $this->prenom = $prenom;
            echo('Je construit mon objet');
        }

        public function __destruct(){
            echo('Je détruit mon objet');
        }

        public function __get($name){
            var_dump($name);
        }

        public function __set($name, $value){
            var_dump($name);
            var_dump($value);
        }

        public function __isset($name){
            var_dump($name);
        }

        public function __unset($name){
            var_dump($name);
        }

        public function __sleep(){
           return [
               "prenom"
           ];
        }

        public function __wakeup(){
          $this->heureReveil = new DateTime();
        }

        public function __toString(){
            return $this->nom.' '.$this->prenom;
        }
    }