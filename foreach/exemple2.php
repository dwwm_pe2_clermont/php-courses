<html>
<head>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>Mes pokemons !</h1>
    <?php
    // Je déclare une variable qui correspond à la page actuelle
    // Elle est nulle pour l'instant
    $page = null;
    // Si jamais dans mon URL je n'ai pas la clé page
    if(!array_key_exists("page", $_GET)){
        // J'admets que je dois afficher la première page
        $page = 1;
    } else {
        // Sinon ma variable page est égale au paramètre dans mon URL
        $page = $_GET["page"];
    }

    $pokemons = [
        [
            "src" => "https://cdn.shopify.com/s/files/1/2500/4214/files/pokemon-pikachu-asleep-i101272.jpg?v=1623085760",
            "titre" => "Pikachu",
            "description" => "Pokemon de type foudre",
            "type" => "electrique",
            "actif" => false,
            "attacks" => [
                "Eclair",
                "Foudre"
            ]
        ],
        [
            "src" => "https://www.pokepedia.fr/images/thumb/0/09/Bulbizarre_de_Sacha.png/800px-Bulbizarre_de_Sacha.png",
            "titre" => "Bulbizar",
            "description" => "Pokemon de type plante",
            "type" => "plante",
            "actif" => false,
            "attacks" => [
                "Fouet lianne"
            ],
        ],
        [
            "src" => "https://www.realite-virtuelle.com/wp-content/uploads/2022/11/charmander-758x426.jpg",
            "titre" => "Salamèche",
            "description" => "Pokemon de type feu",
            "type" => "feu",
            "actif" => true,
            "attacks" => [
                "Flamèche",
                "Griffe",
                "Coup de boule"
            ]
        ],
        [
            "src" => "https://assets.pokemon.com/assets/cms2/img/pokedex/full/012.png",
            "titre" => "Papilusion",
            "description" => "Pokemon de type insecte",
            "type" => "plante",
            "actif" => false,
            "attacks" => [
                "Poudre dodo",
                "Parasport"
            ]
        ],
        [
            "src" => "https://assets.pokemon.com/assets/cms2/img/pokedex/full/037.png",
            "titre" => "Goupix",
            "description" => "Pokemon de type feu",
            "type" => "feu",
            "actif" => false,
            "attacks" => [
                "Flamèche"
            ]
        ],
        [
            "src" => "https://cdn.shopify.com/s/files/1/2500/4214/files/pokemon-pikachu-asleep-i101272.jpg?v=1623085760",
            "titre" => "Pikachu",
            "description" => "Pokemon de type foudre",
            "type" => "electrique",
            "actif" => false,
            "attacks" => [
                "Eclair",
                "Foudre"
            ]
        ],
        [
            "src" => "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/043.png",
            "titre" => "Mysterbe",
            "description" => "Pokemon de type plante",
            "type" => "plante",
            "actif" => false,
            "attacks" => [
                "Poudre dodo",
                "Volvie"
            ]
        ]


    ];
    ?>

    <div class="row">

        <?php


        // Je commence par retrouver l'indice dans le tableau du premier
        // élément à afficher
        $firstElem = ($page-1)*2;
        // Vu que j'affiche 2 éléments je fais un + 2 sur cet index
        $lastElem = $firstElem + 2;

        // Je fais une boucle for qui ira afficher tous les éléments compris
        // entre le premier et le dernier
        for($i = $firstElem; $i<$lastElem; $i++){
            // Je réccupére le pokemon dans mon tableau
            if(array_key_exists($i, $pokemons)){
                $pokemon = $pokemons[$i];

                // J'affiche mon cart (pas changé depuis la derniere version)
                $color = '';

                // Si le type est electrique
                if ($pokemon["type"] == "electrique") {
                    // La variable color est égale à yellow
                    $color = "yellow";
                }

                // Si le type est feu
                if ($pokemon["type"] == "feu") {
                    // La variable est égale à red
                    $color = "red";
                }

                // Si le type est plante
                if ($pokemon["type"] == "plante") {
                    // La variable est égale à green
                    $color = "green";
                }

                // J'ajoute la propriété background-color égal à la valeur de ma variable
                echo('
                <div class="card ' . $color . '" style="width: 18rem;">
                    <img src="' . $pokemon["src"] . '" class="card-img-top" alt="' . $pokemon["titre"] . '">
                <div class="card-body">
                    <h5 class="card-title">' . $pokemon["titre"] . '</h5>
                    <p class="card-text">' . $pokemon["description"] . '</p>
                    ');

                echo('<ul>');
                foreach ($pokemon["attacks"] as $attack) {
                    echo('<li>' . $attack . '</li>');
                }
                echo('</ul>');
                echo('</div>
            </div>
            ');
            }
        }
        ?>


    </div>

    <nav aria-label="Page navigation example" class="mt-3">
        <ul class="pagination">
            <?php
                // Si je suis sur la page n°1, je n'affiche pas le bouton précédent
                if($page!=1){
                    echo('<li class="page-item">
                    <a class="page-link" href="exemple2.php?page=' . $page-1 .'">
                    Previous</a></li>');
                }
            ?>

            <?php
            // Je réccupére mon nombre de page
            // La taille de mon tableau divisé par le nombre d'éléments à afficher
            // J'arrondis à l'entier au dessus le plus  proche
            $nbPages = ceil(count($pokemons)/2);

            for ($i = 1; $i <= $nbPages; $i++) {
                $active = ($i==$nbPages)?'active':'';
                // Si la boucle de mon tableau est égale à la page courante je mets la classe active
                if($i == $page){
                    echo('<li class="page-item active"><a class="page-link" href="exemple2.php?page='.$i.'">' . $i . '</a></li>');
                } else {
                    echo('<li class="page-item"><a class="page-link" href="exemple2.php?page='.$i.'">' . $i . '</a></li>');
                }

            }
            ?>

            <?php
                // Si je suis sur la dernière page je n'affiche pas le bouton suivant
                if($nbPages != $page){
                    echo( '<li class="page-item"><a class="page-link"
                    href="exemple2.php?page='. $page+1 .'">Next</a></li>');
                }
            ?>
          
        </ul>
    </nav>

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
</body>
</html>