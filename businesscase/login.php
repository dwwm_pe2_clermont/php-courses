<?php
    // session_start();
    $error = null;
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if($_POST["username"] == "aurelien" && $_POST["password"] == "aurelien"){
            // J'ajoute mon username dans la session
            $_SESSION["username"] = "aurelien";
            // Je redirige vers la page home
            header("Location: home.php");
        } else {
            $error = "Identifiants invalides";
        }
    }
?>
<html>
<head>
    <?php
    include 'parts/stylesheets.php';
    ?>
</head>
<body>
<div class="container">
    <?php
        include 'parts/nav.php';
    ?>
    <h1>Me connecter !</h1>

    <form method="post">
        <div>
            <label>Nom d'utilisateur</label>
            <input type="text" name="username" class="form form-control">
        </div>

        <div>
            <label>Mot de passe</label>
            <input type="password" name="password" class="form-control">
        </div>

        <?php
            if(!is_null($error)){
                echo('<div class="text-danger">'.$error.'</div>');
            }
        ?>


        <input type="submit" class="btn btn-success mt-2">
    </form>
</div>
<?php
include 'parts/scripts.php'
?>
</body>
</html>