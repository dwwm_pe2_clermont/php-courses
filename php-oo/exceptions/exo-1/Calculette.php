<?php
    class Calculette{
        public function diviser($nombre1, $nombre2){
            $result = null;
            if($nombre2 == 0){
                throw new Exception("Impossible ! Division par 0", 50);
            } else {
                $result = $nombre1/$nombre2;
            }

            return $result;
        }
    }