<?php
    interface CrudInterface extends DbInterface {
        public function findAll();
        public function find($id, $deep=false);
        public function save($obj);
        public function remove($obj);
        public function edit($obj);
    }