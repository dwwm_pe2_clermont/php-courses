<html>
<head>
    <?php
    include 'blocks/stylesheets.php';
    ?>
</head>
<body>
<div class="container">

    <?php
    include 'blocks/header.php';
    include 'blocks/menu.php';
    include_once 'blocks/menu.php';
    ?>
    <h1>Moteur de recherche !</h1>

    <div>
       <form>
           <div>
               <label>Recherche</label>
               <input <?php
                if(array_key_exists("search", $_GET)){
                    echo("value=".$_GET["search"]);
                }
               ?> name="search" type="text" class="form-control">
           </div>

           <div>
               <label>Prix minimum</label>
               <input <?php
               if(array_key_exists("prix_min", $_GET)){
                   echo("value=".$_GET["prix_min"]);
               }
               ?> name="prix_min" type="number" class="form-control">
           </div>

           <div>
               <label>Prix maximum</label>
               <input  <?php
               if(array_key_exists("prix_max", $_GET)){
                   echo("value=".$_GET["prix_max"]);
               }
               ?> name="prix_max" type="number" class="form-control">
           </div>

           <input type="submit" class="m-2 btn btn-success">

       </form>
    </div>

</div>

<?php
include "blocks/javascripts.php";
?>
</body>
</html>