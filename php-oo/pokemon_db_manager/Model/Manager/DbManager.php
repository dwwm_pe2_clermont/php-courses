<?php
abstract class DbManager{
    private $host = 'database';
    private $dbName = 'pokedex';
    private $user = 'root';
    private $password = 'tiger';

    protected $bdd;

    public function __construct(){
        $this->bdd =  new PDO(
            'mysql:host='.$this->host.';dbname='.$this->dbName.';charset=utf8',
            $this->user,
            $this->password);
        // Cette ligne demandera à pdo de renvoyer les erreurs SQL si il y en a
        $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }


}