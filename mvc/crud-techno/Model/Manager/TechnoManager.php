<?php
class TechnoManager extends DbManager{
    public function getAll(){
        $arrayObjet = [];

        $query = $this->bdd->query("SELECT * FROM technos");

        $results = $query->fetchAll();

        foreach ($results as $result){
            $arrayObjet[] =
                new Techno($result["id"], $result["nom"],
                    $result["image"], $result["categorie"]);
        }

        return $arrayObjet;
    }

    public function getOne($id){
        $query = $this->bdd->prepare("SELECT * FROM technos WHERE id = :id");
        $query->execute(["id"=> $id]);
        $result = $query->fetch();

        $techno = null;

        if($result){
            $techno = new Techno($result["id"], $result["nom"], $result["image"], $result["categorie"]);
        }

        return $techno;
    }

    public function remove($id){
        $query = $this->bdd->prepare("DELETE FROM technos WHERE id = :id");
        $query->execute(["id"=> $id]);
    }

    public function insert(Techno $techno)
    {
        $query = $this->bdd->prepare("INSERT INTO technos (nom, image, categorie) 
        VALUES (:nom, :image, :categorie)"
        );

        $query->execute([
            "nom"=> $techno->getNom(),
            "image"=> $techno->getImage(),
            "categorie"=> $techno->getCategory()
        ]);
    }

    public function update(Techno $technoEdit)
    {
        $query = $this->bdd->prepare("UPDATE technos 
        SET nom = :nom, image = :image, categorie=:categorie 
        WHERE id = :id");

        $query->execute([
            "nom"=> $technoEdit->getNom(),
            "image"=> $technoEdit->getImage(),
            "categorie"=> $technoEdit->getCategory(),
            "id"=> $technoEdit->getId()
        ]);

    }
}