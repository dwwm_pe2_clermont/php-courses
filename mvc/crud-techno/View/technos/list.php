<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <?php
    include 'View/parts/nav.php'
    ?>
    <a class="btn btn-success" href="index.php?controller=techno&action=add">Ajouter une nouvelle techno</a>
<h1>Les technos en BDD</h1>

    <?php echo($this->user->getEmail()); ?>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">Photo</th>
            <th scope="col">Catégorie</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
         foreach ($allTechnologies as $technology){
             echo('<tr>
            <th scope="row">'.$technology->getId().'</th>
            <td>'.$technology->getNom().'</td>
            <td><img style="max-width: 100px;" class="img img-thumbnail" 
            src="'.$technology->getImage().'"></td>
            <td>'.$technology->getCategory().'</td>
            <td>
            <a href="index.php?controller=techno&action=detail&id='.$technology->getId().'">
            Voir le détail
            </a><br>
            <a href="index.php?controller=techno&action=delete&id='.$technology->getId().'">
            Supprimer
            </a><br>
            <a href="index.php?controller=techno&action=edit&id='.$technology->getId().'">Editer</a>
            </td>
        </tr>');
         }
        ?>

        </tbody>
    </table>

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>