<?php
    class Cercle implements FormeInterface {
        private $rayon;

        public function __construct($rayon){
            $this->rayon = $rayon;
        }

        public function calculSurface()
        {
            return pi() * ($this->rayon * $this->rayon);
        }
    }
?>