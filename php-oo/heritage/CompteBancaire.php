<?php
class CompteBancaire{
    private $numeroCompte;

    public function __construct($numeroCompte){
        $this->numeroCompte = $numeroCompte;
    }

    public function getNumeroCompte(){
        return $this->numeroCompte;
    }
}