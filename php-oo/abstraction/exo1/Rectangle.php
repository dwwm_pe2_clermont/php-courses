<?php
 class Rectangle implements FormeInterface {
     private $largeur;
     private $longueur;

     public function __construct($largeur, $longueur)
     {
         $this->largeur = $largeur;
         $this->longueur = $longueur;
     }


     public function calculSurface()
     {
         return $this->largeur * $this->longueur;
     }
 }