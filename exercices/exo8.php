<?php

// On a notre liste d'emails
$emails = [
    'aureliendelorme1@gmail.com',
    "aurelien@laposte.fr",
    "toto@gmail.com"
];


// Je cré un tableau clé valeur
// Ce tableau aura pour clé le nom de domaine
// Pour l'instant ce tableau aura la valeur 0
foreach ($emails as $email){
    $domaines[explode("@", $email)[1]] = 0;
}


// Je re parcours mes emails
foreach ($emails as $item){
    // Je réccupére mon nom de domaine
    $domainName = explode("@", $item)[1];
    $domaines[$domainName] = $domaines[$domainName] + 1;
}

foreach ($domaines as $key=>$value){
    $pourcentage = $value/count($emails)*100;
    echo('Il y a '.$value.' adresse sur le domaine'  .$key .
        ' ce qui représente '.$pourcentage.'% </br>');
}


