<?php
// Je déclare les 3 variables prises dans le discord
$password="Mystrongpass63";
$firstname = "Aurélien";
$lastname = "Delorme";

// Je test mon mot de passe avec une regex réalisée à partir
// https://www.ipgirl.com/5164/les-expressions-rationnelles-pour-le-mot-de-passe-doivent-contenir-au-moins-huit-caracteres-au-moins-un-chiffre-et-a-la-fois-des-lettres-majuscules-et-minuscules-et-des-caracteres-speciaux.html
// https://regex101.com/
$testPassword =
    preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/', $password);

// Si la regex est ok
if($testPassword){
    // Je vérifie que le mot de passe ne contient ni mon prénom
    // no mon nom de famille
    if(stristr($password, $firstname) === FALSE &&
        stristr($password, $lastname) === FALSE
    ) {

        // Tout est ok
        echo('OK le mot de passe est bon');
    } else {
        // PAS OK il y a mon nom ou mon prénom dans le mot de passe
        echo('Impossible, le mot de passe contient votre identité');
    }

    // Le mot de passe ne match pas avec ma regex
    // Il n'est pas assez complexe
} else {
    echo('Le mot de passe n\'est pas bon' );
}