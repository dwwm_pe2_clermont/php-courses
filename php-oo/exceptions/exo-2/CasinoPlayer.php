<?php
class CasinoPlayer{
    private $nom;
    private $prenom;
    private $age;

    public function __construct($nom, $prenom, $age){
        if($age<18){
            throw new Exception("Vous devez avoir 18 ans pour jouer");
        }
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->age = $age;
    }
}