<?php
include 'functions.php';
checkConnection();
$pdo = dbConnect();

// On selectionne tous nos pokemons
$query = $pdo->prepare("SELECT * FROM pokemon 
    INNER JOIN type ON pokemon.type_id = type.id
WHERE type_id = :typeId");
// On les réccupére sous forme de tableau
$query->execute(["typeId"=> $_GET["type"]]);
$resultats = $query->fetchAll();

?>

<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous"/>
</head>
<body>

<div class="container">
    <?php
    include 'parts/navbar.php'
    ?>

    <?php
    // Si il n'y a pas de résultat, j'affiche un message qui indique que le pokemon n'existe pas
    if(!$resultats){
        echo('<div class="text-danger text-center">Ce type de pokémon n\'existe pas il a peut être été supprimé </div>');
    }
    // Sinon, j'affiche la page du pokemon
    else {
    ?>

    <h1>Les pokemons de type <?php echo($resultats[0]["libelle"]);?> !</h1>
    <div class="row">
        <?php
        // On parcours notre tableau de pokemon pour les afficher !
        foreach ($resultats as $resultat){
            echo('<div class="card" style="width: 18rem;">
  <img src="uploads/images_pokemon/'.$resultat["image"].'" class="card-img-top" alt="Pokemon '.$resultat["nom"].'">
  <div class="card-body">
    <h5 class="card-title">'.$resultat["nom"].'</h5>
    <a href="detail.php?id='.$resultat[0].'" class="btn btn-primary">Voir la ficher détaillée</a>
  </div>
</div>');
        }
        ?>
    </div>
    <?php
    }
    ?>

</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>
</html>

