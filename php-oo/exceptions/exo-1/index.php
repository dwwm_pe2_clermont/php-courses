<?php
    require "Calculette.php";

    $calculette = new Calculette();
    $result = null;
    try {
       $result =  $calculette->diviser(10,2);
    } catch (\Exception $e){
        echo($e->getMessage().'</br>');
        echo($e->getCode().'</br>');
        echo($e->getFile().'</br>');
        echo($e->getLine().'</br>');
    }

    var_dump($result);
?>