<?php
    // Je cré une variable date qui correspond à la date du jour
    $date = date_create();
    $timestampOrigin = $date->getTimestamp();
    echo('Timestamp actuel : '. $date->getTimestamp()."</br>");
    // Je cré un interval que j'ajouterais à ma date
    // En loccurence un interval de 10 ans
    $interval = DateInterval::createFromDateString('10 years');

    // J'ajoute mon interval à ma date
    $date->add($interval);

    // J'affiche le jour à cette date
    $timeStampMaj = $date->getTimestamp();
    echo("Jour de l'année : ".$date->format("l")."</br>");
    echo("Timestamp dans 10 ans : " . $date->getTimestamp()."</br>");
    echo('En 10 ans, il y a : '. $timeStampMaj - $timestampOrigin);
?>