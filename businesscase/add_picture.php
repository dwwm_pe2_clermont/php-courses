<?php
// session_start();

// Je définie les extensions que j'autorise
$allowedExtension = ["image/jpeg", "image/png"];
$errors = [];
var_dump($_FILES);
die();
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Je vérifie qu'il n'y a pas d'erreurs d'upload
    if($_FILES["photos"]["error"] != 0){
        $errors[] = "Une erreur inconnue est survenue";
    }
// Je vérifie que j'ai le droit d'uploader ce type de fichier
    if(in_array($_FILES["photos"]["type"], $allowedExtension)){
        // Je vérifie que mon fichier n'est pas trop lourd
        if($_FILES["photos"]["size"]>2097152){
            $errors[] = "Impossible cette photo est trop grosse";
        }
    } else {
        $errors[] = "Le format de l'image n'est pas bon. Ajoutez un jpg ou un png";
    }

// Si j'ai pas d'erreurs
    if(count($errors) == 0){
        // J'upload mon fichier
        move_uploaded_file($_FILES["photos"]["tmp_name"], "uploads/".uniqid().'-'.$_FILES["photos"]["name"]);
        // Je redirige mon utilisateur vers la page d'acceuil
        header("Location: home.php");
    }

}

?>
<html>
<head>
    <?php
    include 'parts/stylesheets.php';
    ?>
</head>
<body>
<div class="container">
    <?php
    include "parts/nav.php";
    ?>
    <h1>Ajouter une image</h1>

    <form method="post" enctype="multipart/form-data">
        <input type="file" class="form-control" name="photos">
        <input type="submit" class="btn btn-success mt-2">

        <div class="text-danger">
            <?php
            if(count($errors) != 0){
            ?>
            <ul>
                <?php
                    foreach ($errors as $error){
                        echo('<li>'.$error.'</li>');
                    }
                ?>
            </ul>
            <?php
            }
            ?>
        </div>
    </form>

    <?php
    include "parts/cookie-content.php";
    ?>
</div>
<?php
include 'parts/scripts.php'
?>
</body>
</html>