<html>
    <head>

    </head>

    <body>
        <h1>Les images dans le dossier images</h1>
        <ul>
            <?php
            // Je réccupére tous les fichiers contenu dans le dossier images
            $files = scandir("images");
            // Pour chacun de ces fichiers
            foreach ($files as $file){
                // Si le nom est différent de . et .. (les actions de retour en arrière)
                if($file != "." && $file != ".."){
                    // J'affiche mon image dans une li
                    echo('<li><img src="images/'.$file.'"></li>');
                }

            }
            ?>
        </ul>
    </body>
</html>
