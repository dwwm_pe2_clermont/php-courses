<?php
    $panier = [];
    // Je réccupére tous les articles dans mon panier
    // Qui sont stocké dans mes cookies
    if(array_key_exists("panier", $_COOKIE)){
        // Je transforme la chaine de caractères contenant mon panier
        // en tableau (obligatoire puisque les cookies sont distribués dans un fichier text)
        $panier = json_decode($_COOKIE["panier"], true);

    }
?>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <?php
        include "nav.php"
    ?>
    <h1>Mon panier !</h1>

    <?php
        // Si la taille de mon tableau de cookie est de 0
        // celà veut dire que mon panier est vide !
        if(count($panier) == 0){
            echo('<h2>Votre panier est vide !</h2>');
        }
    ?>
    <div class="row">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nom</th>
                <th scope="col">Quantite</th>
                <th scope="col">Prix Unitaire</th>
                <th scope="col">Prix</th>
            </tr>
            </thead>
            <tbody>
            <?php
                // J'initialise une variable qui définira le prix total du
                // panier (on part de 0)
                $price = 0;
                // Je parcours chaque item de mon panier pour afficher
                // une nouvelle ligne dans un tableau

                foreach ($panier as $itm){
                    echo(' <tr>
                <th scope="row">'.$itm["id"].'</th>
                <td>'.$itm["nom"].'</td>
                <td>'.$itm["quantite"].'</td>
                <td>'.$itm["prix"].'</td>
                 <td>'.$itm["prix"]*$itm["quantite"].'</td>
            </tr>');
                    // J'augmente la variable du prix en l'additionnant elle même
                    // avec le prix unitaire multiplié par la quantité de chaque
                    // element présent dans mon panier
                    $price = $price + $itm["prix"]*$itm["quantite"];
                }
            ?>

            </tbody>
        </table>

        <h3>
            Prix total : <?php
            // J'affiche le prix total de mon panier
            echo($price);?>
        </h3>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>