<?php
class CarManager extends DbManager{
    public function getAll(){
        $query = $this->bdd->query("SELECT * FROM cars");
        $resultsArray = $query->fetchAll();

        $objectArray = [];

        foreach ($resultsArray as $result){
            $objectArray[] = new Car(
                $result["id"], $result["marque"], $result["model"],
                $result["energy"], $result["automatic"],
                $result["picture"]);
        }

        return $objectArray;
    }

    public function insert(Car $car)
    {
        var_dump($car);
        $query = $this->bdd->prepare("INSERT INTO cars
    (marque, model, energy, automatic, picture)
    VALUES (:marque, :model, :energy, :automatic, :picture)");

        $query->execute(
            [
                "marque"=> $car->getMarque(),
                "model"=> $car->getModel(),
                "energy" => $car->getEnergy(),
                "automatic" => $car->getAutomatic(),
                "picture"=> $car->getPicture()
            ]
        );
    }

    public function getOne($id)
    {
        $query = $this->bdd->prepare("SELECT * FROM cars WHERE id = :id");

        $query->execute([
            "id"=> $id
        ]);

        $result = $query->fetch();


        $object = null;

        if($result){
            $object = new Car( $result["id"], $result["marque"], $result["model"],
                $result["energy"], $result["automatic"],
                $result["picture"]);
        }


        return $object;
    }

    public function delete($id)
    {
        $query = $this->bdd->prepare("DELETE FROM cars WHERE id = :id");
        $query->execute([
            "id"=> $id
        ]);
    }
}