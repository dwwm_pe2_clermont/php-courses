<?php
    // session_start
    require 'functions.php';
    restrictAccess();
    $pdo = dbConnect();
    $request = $pdo->prepare("DELETE FROM player WHERE id = :id");
    $request->execute(
        ["id"=> $_GET["id"]]);
    header("Location: admin-index.php");
?>