<?php

spl_autoload_register(function($classname){
    require $classname.'.php';
});

$user = new User("Aurélien", "Delorme");
var_dump($user);

$product = new Product("Iphone");
var_dump($product);

$animal = new Animal();