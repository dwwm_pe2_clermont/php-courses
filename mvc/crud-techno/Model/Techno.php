<?php
class Techno{

    public static $allowedTechno = ["Front end", "Back end", "Base de données"];

    private $id;
    private $nom;
    private $image;
    private $category;

    /**
     * @param $id
     * @param $nom
     * @param $image
     * @param $category
     */
    public function __construct($id, $nom, $image, $category)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->image = $image;
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

}