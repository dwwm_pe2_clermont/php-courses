<?php
 class CompteParticulier extends CompteBancaire{
     private $nomTitulaire;
     private $prenomTitulaire;
     private $numSecuTitulaire;

     public function __construct($numeroCompte, $nomTitulaire,
                                 $prenomTitulaire, $numSecuTitulaire){
         parent::__construct($numeroCompte);
         $this->nomTitulaire = $nomTitulaire;
         $this->prenomTitulaire = $prenomTitulaire;
         $this->numSecuTitulaire = $numSecuTitulaire;
     }

     public function getNomTitulaire(){
         return $this->nomTitulaire;
     }

     public function getPrenomTitulaire(){
         return $this->prenomTitulaire;
     }

     public function getNumSecuTitulaire(){
         return $this->numSecuTitulaire;
     }
 }