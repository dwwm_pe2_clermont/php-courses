<?php
    // Je cré la date du jour
    $aujourdhui = date_create();
    // Je cré ma date d'anniversaire
    $birthdate = date_create("1993-03-30");
    // Je calcul la différence entre les deux dates
    $difference = date_diff($aujourdhui, $birthdate);

    // J'affiche le nombre de jours entre ces deux dates
    echo('Je suis né il y a ' . $difference->days." jours </br>");

    // J'affiche le nombre de jour multiplié par 24 (les jours complet)
    // J'ajoute le nombre d'heure incomplete ecoulées
    echo('Je suis né il y a ' . $difference->days * 24 + $difference->h. " heures</br>");

    // Même procédé sauf que je multiplie par 60 mes jours et mes heures
    // J'ajoute les minutes écoulées entre les deux
    $minutes = $difference->days * 24 * 60+ $difference->h  * 60 + $difference->i;
    echo('Je suis né il y a ' . $minutes . " minutes</br>");

    $secondes = $difference->days * 24 * 60 * 60+ $difference->h  * 60 * 60+ $difference->i * 60 + $difference->s;
    echo('Je suis né il y a ' . $secondes . " secondes</br>");

    var_dump($difference);