<?php
    class CompteProfessionnel extends CompteBancaire {
        private $numeroSiret;
        private $raisonSociale;

        public function __construct($numeroCompte, $numeroSiret, $raisonSociale)
        {
            parent::__construct($numeroCompte);
            $this->numeroSiret = $numeroSiret;
            $this->raisonSociale = $raisonSociale;
        }

        public function getNumeroSiret(){
            return $this->numeroSiret;
        }

        public function getRaisonSociale(){
            return $this->raisonSociale;
        }

    }
?>