<?php
    // session_start
    $error = null;
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if($_POST["username"] == "ddechamps" && $_POST["password"] = "ddechamps"){
            $_SESSION["connecte"] = true;
            header('Location: admin-index.php');
        } else {
            $error = 'Identifiants invalides';
        }
    }
?>
<html>
<head>
    <?php
    include 'parts/stylesheets.php';
    ?>
</head>
<body>

<div class="container">
    <h1>Accès restreint : Veuillez vous connecter</h1>
    <form method="post">
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" name="username">
        </div>

        <div class="form-group">
            <label>Mot de passe</label>
            <input type="password" class="form-control" name="password">
        </div>

        <?php
        if(!is_null($error)){
            echo('<div class="text-danger">'.$error.'</div>');
        }
        ?>

        <input class="btn btn-success mt-3" type="submit">


    </form>
</div>

<?php
include 'parts/javascripts.php';
?>
</body>
</html>