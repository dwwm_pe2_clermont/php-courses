<?php
    abstract class Produit implements Promotion{
        private $nom;
        private $price;

        public function __construct($nom, $price){
            $this->nom = $nom;
            $this->price = $price;
        }

        public function getNom(){
            return $this->nom;
        }

        public function montantPromotion($montant)
        {
            if($this->getPrix()-$montant<0){
                throw new PrixNegatifException();
            } else {
                $this->setPrix($this->getPrix() - $montant);
            }
        }

        public function setNom($nom){
            $this->nom = $nom;
        }

        public function getPrix(){
            return $this->price;
        }

        public function setPrix($prix){
            $this->price = $prix;
        }
    }
?>