<?php
    class Personne {
        protected $nom;
        protected $prenom;
        private $age;

        public function __construct($nom, $prenom, $age){
            $this->nom = $nom;
            $this->prenom = $prenom;
            $this->age = $age;
        }

        public function getNom(){
            return $this->nom;
        }

        public function setNom($nom){
            $this->nom = $nom;
        }

        public function getPrenom(){
            return $this->prenom;
        }

        public function setPrenom($prenom){
            $this->prenom = $prenom;
        }

        protected function getAge(){
            return $this->age;
        }

        protected function setAge($age){
            $this->age = $age;
        }


    }
?>