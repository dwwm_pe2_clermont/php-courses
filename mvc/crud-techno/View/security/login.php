<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <a class="btn btn-success mb-3" href="index.php?controller=security&action=register">Créer un compte</a>
    <h1>Me connecter</h1>

    <form method="post">
        <div class="form-group">
            <label>Email</label>
            <input name="email" class="form-control">
        </div>

        <div class="form-group">
            <label>Mot de passe</label>
            <input type="password" name="password" class="form-control">
        </div>

        <input type="submit" class="btn btn-success mt-3">
    </form>

    <?php if($error){
        echo('<div class="text-danger">Identifiants incorrects</div>');
    }
    ?>
</div>
</body>
</html>
