<?php
// Pour utiliser les variables de session, vous devez utiliser
// session_start();
// session_start();
// Session destroy détruit notre session, l'utilisateur est donc
// déconecté
session_destroy();
// Je redirige l'utilisateur vers la page de login
header("Location: login.php");