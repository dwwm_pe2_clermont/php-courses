<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <?php
    include 'View/parts/nav.php'
    ?>

    <a class="btn btn-success" href="index.php?controller=techno&action=list">Retour</a>
    <h1>Ajouter une techno !</h1>
    <form method="post">
        <div class="form-group">
            <label>Nom de la techno</label>
            <input type="text" value="<?php if(array_key_exists("nom", $_POST)){echo($_POST["nom"]);}?>" name="nom"
                   class="form-control <?php if(array_key_exists("nom", $errors)){echo('is-invalid');}?>">
            <?php
            if(array_key_exists("nom", $errors)){
                echo('<div class="invalid-feedback">'.$errors["nom"].'</div>');
            }
            ?>
        </div>

        <div class="form-group">
            <label>Image de la techno</label>
            <input type="text"
                   value="<?php if(array_key_exists("image", $_POST)){echo($_POST["image"]);}?>"
                   name="image" class="form-control <?php if(array_key_exists("image", $errors)){echo('is-invalid');}?>">
            <?php
                if(array_key_exists("image", $errors)){
                    echo('<div class="invalid-feedback">'.$errors["image"].'</div>');
                }
            ?>
        </div>

        <div class="form-group">
            <label>Catégorie</label>
            <select name="categorie" class="form-select
            <?php if(array_key_exists("categorie", $errors)){echo('is-invalid');}?>"">
                <option></option>
                <?php
                    foreach (Techno::$allowedTechno as $techno){
                        $selected = '';
                        if($_POST["categorie"] == $techno){
                            $selected = 'selected';
                        }
                        echo('<option '.$selected.' value="'.$techno.'">'.$techno.'</option>');
                    }
                ?>
            </select>

            <?php
            if(array_key_exists("categorie", $errors)){
                echo('<div class="invalid-feedback">'.$errors["categorie"].'</div>');
            }
            ?>

        </div>


        <input type="submit" class="btn btn-success mt-3">
    </form>
</div>
</body>
</html>
