<?php
// session_start();

if(!array_key_exists("username", $_SESSION)){
    header("Location: login.php");
}
?>
<html>
<head>
    <?php
    include 'parts/stylesheets.php';
    ?>
</head>
<body>
<div class="container">
    <?php
    include "parts/nav.php";
    ?>
    <h1>Le compte de <?php echo($_SESSION["username"]);?></h1>

    <?php
    include "parts/cookie-content.php";
    ?>
</div>
<?php
include 'parts/scripts.php'
?>
</body>
</html>