<?php
    // N'oubliez le session_start

    // Je vérifie si j'ai un utilisateur connecté
    if(!array_key_exists("username", $_SESSION)){
        // Si personne n'est connecté, je redirige l'utilisateur
        // vers la page de login
        header('Location: login.php');
        exit;
    }
?>

<html>
<head>

</head>
<body>
<!-- J'affiche Bonjour suivi du prénom de l'utilisateur conecté -->
<h1>Bonjour <?php echo($_SESSION["username"]); ?></h1>
<a href="logout.php">Me déconnecter</a>
</body>
</html>
