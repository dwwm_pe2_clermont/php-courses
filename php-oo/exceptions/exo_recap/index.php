<?php
    require "PrixNegatifException.php";
    require "Produit.php";
    require "Promotion.php";
    require "ProduitAlimentation.php";
    require "ProduitElectronique.php";
    require "ProduitVetement.php";

    $vetement = new ProduitVetement("Pull de noël", 20);

    var_dump($vetement);

    $produitElectronique = new ProduitElectronique("Iphone", 600);
    var_dump($produitElectronique);

    $produitAlimentaire = new ProduitAlimentation("Pizza", 8);
    $produitAlimentaire->montantPromotion(10);
    var_dump($produitAlimentaire);

?>