<?php
class SecurityController {

    private $userManager;

    public function __construct(){
        $this->userManager = new UserManager();
    }

    public function register(){
        $errors = [];
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            // Traitements du formulaires
            // Vérification des erreurs
            $errors = $this->isValidRegisterForm();

            if(count($errors) == 0){
                $user = new User(null,
                    $_POST["email"],
                    password_hash($_POST["password"], PASSWORD_DEFAULT));

                $this->userManager->add($user);

                header("Location: index.php?controller=security&action=login");

            }
        }
        require "View/security/register.php";
    }

    private function isValidRegisterForm(){
        $errors = [];

        if(empty($_POST["email"])){
            $errors["email"] = "Veuillez saisir un email";
        } elseif(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
            $errors["email"] = "Votre email n'est pas valide";
        } elseif (!is_null($this->userManager->findByEmail($_POST["email"]))){
            $errors["email"] = "Ce compte existe déjà";
        }

        if(empty($_POST["password"])){
            $errors["password"] = "Veuillez saisir un mot de passe";
        } elseif(strlen($_POST["password"])<9){
            $errors["password"] = "Le mot de passe doit faire au moins 8 caractères";
        }
        if(empty($_POST["confirm_password"])){
            $errors["confirm_password"] = "Veuillez confirmer votre mot de passe";
        }

        if($_POST["password"] != $_POST["confirm_password"]){
            $errors["confirm_password"] = "Les mots de passe ne sont pas identiques";
        }

        return $errors;
    }

    public function login()
    {
        $error = false;

        if($_SERVER["REQUEST_METHOD"] == "POST"){
            // Je vais chercher un utilisateur en fonction de son email
            $user = $this->userManager->findByEmail($_POST["email"]);

            // Je vais vérifier que le mot de passe est le bon
            if($user){
                if(password_verify($_POST["password"], $user->getPassword())){
                    // session_start();
                    $_SESSION["user"] = serialize($user);
                    header("Location: index.php?controller=techno&action=list");
                } else {
                    $error = true;
                }
            } else {
                $error = true;
            }

        }

        require "View/security/login.php";
    }

    public function logout()
    {
        // session_start();
        session_destroy();
        header("Location: index.php?controller=security&action=login");
    }


}